import { ChildProcess, ChildProcessWithoutNullStreams, exec, execSync, spawn } from 'child_process';
import { logger } from './logger-service';

export class ProcessService {
  private static processRepository = new Map<string, ChildProcess>();

  private constructor() {
    /** Private constructor is used to make the class act like a singleton! */
    /** Deliberately empty. */
  }

  public static createProcess(filePath: string, fileName: string) {
    if (this.isRunning(fileName)) {
      logger.info(`The process is already running ${fileName}.`);
      return 'The process is already running ' + fileName;
    }
    const instance = spawn('ts-node', [filePath]);
    // const instance = spawn(`ts-node ${filePath}`, { stdio: 'inherit', shell: true });
    // Maybe also check for errors from the process and create a notification so we can debug if necessary.

    instance.on('exit', (code) => {
      logger.info(`Child process exited with code ${code} ==> ${fileName}`);
      this.processRepository.delete(fileName);
    });

    instance.on('error', (error) => {
      logger.error(`Error: ${fileName} logged an error -> ${error}`);
      this.processRepository.delete(fileName);
    });

    this.processRepository.set(fileName, instance);
    logger.info(`The process is started successfully ${fileName}.`);
    return 'The process is started successfully ' + fileName;
  }

  public static isRunning(fileName: string) {
    return this.processRepository.has(fileName);
  }

  public static endProcess(fileName: string) {
    if (this.isRunning(fileName)) {
      const _process = this.processRepository.get(fileName);
      if (_process?.pid) {
        const success = process.kill(_process.pid);
        if (success) {
          _process.once('exit', (code) => {
            this.processRepository.delete(fileName);
          });
          logger.info(`Process killed successfully ${fileName}.`);
          return 'Process has been added to the kill queue.';
        }
        logger.info(`Process could not be killed ${fileName}.`);
        return 'Process could not be killed.';
      }
      if (_process) {
        logger.info(`Process is found but PID is undefined ${fileName}.`);
        return 'Process is found but PID is undefined!';
      }
    }
    logger.info(`Process could not be found! ${fileName}.`);
    return 'Process could not be found!';
  }

  public static numOfRunningProcesses() {
    logger.info(`Number of running processes: ${this.processRepository.size}`);
    return this.processRepository.size;
  }

  public static stopAllBots() {
    for (const _process of this.processRepository.values()) {
      _process.kill();
    }
    this.processRepository.clear();
    logger.info(`Process repository is cleaned.`);
  }

  public static stateOfProcesses(
    arrayOfProcesses: Array<{ processId: string; status: boolean }>
  ): Array<{ processId: string; status: boolean }> {
    for (const procesess of arrayOfProcesses) {
      if (this.isRunning(procesess.processId)) procesess.status = true;
      else procesess.status = false;
    }
    return arrayOfProcesses;
  }
}
