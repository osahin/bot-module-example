import 'dotenv/config';
import { app } from './app';
import { logger } from './services/logger-service';

const PORT = process.env.PORT || 80;

/**
 * The application needs to be started in this way for enabling Jest tests.
 */
const start = () => {
  app.listen(PORT, () => {
    logger.info(`Initialization successful -> Listening on port ${PORT}!`);
  });
};

start();
