import 'dotenv/config';
import express from 'express';
import { MinioService } from './services/minio-service';
import { TSGenerator } from './generators/ts-generator';
import fetch from 'node-fetch';

if (
  !process.env.MINIO_ENDPOINT ||
  !process.env.ACCESS_KEY ||
  !process.env.SECRET_KEY ||
  !process.env.MINIO_PORT ||
  !process.env.BUCKET_NAME ||
  !process.env.FILE_PATH ||
  !process.env.ENDPOINT_PORT ||
  !process.env.ENDPOINT_URL
) {
  throw new Error(
    'Make sure MINIO_ENDPOINT, ACCESS_KEY, SECRET_KEY, MINIO_PORT, BUCKET_NAME, FILE_PATH, ENDPOINT_PORT, and ENDPOINT_PORT  environment variables are defined.'
  );
}

const PORT = process.env.PORT || 80;

const app = express();

const minioService = new MinioService(
  {
    endPoint: process.env.MINIO_ENDPOINT,
    port: Number(process.env.MINIO_PORT),
    useSSL: false,
    accessKey: process.env.ACCESS_KEY,
    secretKey: process.env.SECRET_KEY,
  },
  process.env.BUCKET_NAME,
  process.env.FILE_PATH
);

minioService.on('uploaded', async (fileName) => {
  try {
    await fetch(`http://${process.env.ENDPOINT_URL}:${process.env.ENDPOINT_PORT}/start-bot/${fileName}`);
  } catch (error) {
    console.error(error);
  }
});

app.get('/generate-ts/:id', async (req, res) => {
  const id = req.params.id;
  // Add checks on id.
  // id.length < SOME_NUMBER
  const tsGenerator = new TSGenerator(id, minioService);
  await tsGenerator.generate();
  res.send('Bot id ' + id);
});

app.listen(PORT, () => {
  console.log(`Bot Core listening at ${PORT}.`);
});
