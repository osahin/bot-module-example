export function start(botToken: string, botId: string): string {
  return `require('dotenv').config();
import { Composer, Context, Markup, Scenes, session, Telegraf } from 'telegraf';
import { CallbackQuery, InlineKeyboardButton, Update, User } from 'typegram';
import { telegrafThrottler } from 'telegraf-throttler';
import fetch from 'node-fetch';

if (!process.env.SAVE_ENDPOINT || !process.env.SAVE_PORT) {
  throw new Error('Make sure SAVE_ENDPOINT and SAVE_PORT environment variables are defined.');
}

const MAGIC_ANSWER = 'TO_BE_UPDATED';

async function saveToDb(body: MySession) {
  const botData = {
    id: '${botId}',
    token: botToken,
    conversation: body.userData,
    date_time: Date.now(),
    user: body.user,
  };
  fetch(\`http://\${process.env.SAVE_ENDPOINT}:\${process.env.SAVE_PORT}/api/conversation/create\`, {
    method: 'post',
    body: JSON.stringify(botData),
    headers: { 'Content-Type': 'application/json' },
  }).catch((error) => {
    console.error(error);
  });
}

async function setFileLink(ctx: MyContext) {
  if (ctx.session.userData) {
    for (const conversation of ctx.session.userData) {
      const data = conversation.answer.photo || conversation.answer.voice;
      if (data && Array.isArray(data)) {
        for (const answer of data) {
          if (answer.file_id) {
            answer.file_link = await ctx.telegram.getFileLink(answer.file_id);
          }
        }
      } else if (data && data.file_id) {
        data.file_link = await ctx.telegram.getFileLink(data.file_id);
      }
    }
  }
}

async function setUserPhotoLink(ctx: MyContext) {
  const userPhoto = await ctx.telegram.getUserProfilePhotos(
    ctx.session.user.id
  );
  if (userPhoto.photos.length > 0 || userPhoto.total_count <= 0) return;
  for (const photo of userPhoto.photos[0]) {
    if (!ctx.session.user.photos) ctx.session.user.photos = [];
    ctx.session.user.photos.push(
      (await ctx.telegram.getFileLink(photo.file_id)).href
    );
  }
}

interface CustomUser {
  id: number;

  is_bot: boolean;

  first_name: string;

  last_name?: string;

  username?: string;

  language_code?: string;

  is_premium?: true;

  added_to_attachment_menu?: true;

  photos?: string[];
}

interface UserData {
  question: { text: string; date_time: Date };
  answer: any;
}

interface MySession extends Scenes.WizardSession {
  userData: UserData[];
  isSpecial: boolean;
  user: CustomUser;
}

interface MyContext extends Context {
  myContextProp: string;
  session: MySession;
  scene: Scenes.SceneContextScene<MyContext, Scenes.WizardSessionData>;
  wizard: Scenes.WizardContextWizard<MyContext>;
}

interface ReplyMarkupQuery {
  reply_markup: { inline_keyboard: InlineKeyboardButton.CallbackButton[][] };
}

const botToken = '${botToken}';

const bot = new Telegraf<MyContext>(botToken);

const throttler = telegrafThrottler();

bot.use(throttler);

function updateSession(userData: UserData[], answer: any) {
  let update = userData.find((x) => x.answer == MAGIC_ANSWER)!;
  update.answer = {...answer, date_time: Date.now()};
  for (const data of userData) {
    if (data.answer == MAGIC_ANSWER) {
      throw new Error(\`\${MAGIC_ANSWER} found in updateSession!\`);
    }
  }
}

function clearExtraSession(userData: UserData[]) {
  for (let i = 0; i < userData.length; i++) {
    if (userData[i].answer === MAGIC_ANSWER) {
      userData.splice(i, 1);
      return;
    }
  }
}

function checkMistake(userData: UserData[]) {
  let update = userData.find((x) => x.answer == MAGIC_ANSWER)!;
  if (update) {
    throw new Error(\`checkMistake found \${MAGIC_ANSWER} while the text is not there!\`);
  }
}

function checkAndUpdateSession(ctx: MyContext) {
  const text =
    (ctx.message as { text: string })?.text || (ctx.update as Update.CallbackQueryUpdate).callback_query?.data;
  if (!text && !ctx.session.isSpecial) {
    ctx.session.isSpecial = true;
    return false;
  }
  if (ctx.wizard.cursor == 0) {
    const message = (ctx.update as Update.CallbackQueryUpdate)?.callback_query?.message as ReplyMarkupQuery;
    const element = message.reply_markup.inline_keyboard[0].find((val) => val.callback_data === text);
    updateSession(ctx.session.userData, { text: element?.text ?? 'COULD_NOT_ACQUIRE_TEXT' });
  } else {
    if (text) {
      updateSession(ctx.session.userData, {text});
    } else {
      checkMistake(ctx.session.userData);
    }
  }
  ctx.session.isSpecial = false;
  return true;
}

async function cancelConversation(ctx: MyContext) {
  await ctx.replyWithMarkdown('You have \`Cancelled\` the conversation.');
  // We can also reset the whole thing. Maybe in the /start command or /reset command.
  // return await ctx.scene.reset();
  return await ctx.scene.leave();
}

async function functionHandler({
  ctx,
  message,
  markup,
  isEndNode = false,
}: {
  ctx: MyContext;
  message: string;
  markup?: any;
  isEndNode?: boolean;
}) {
  if (checkAndUpdateSession(ctx) == false) {
    clearExtraSession(ctx.session.userData);
    await ctx.reply('Wrong Input! Please try again.');
    const prevFunction = ctx.wizard.back().step as Function;
    return prevFunction(ctx);
  }
  if (isEndNode) {
    ctx.session.userData.push({ question: {text:message,date_time:new Date()}, answer: 'END_CONVERSATION' });
    await ctx.reply(message, markup ?? { reply_markup: { remove_keyboard: true } });
    if (ctx.message) {
      ctx.session.user = ctx.message.from;
    } else if ((ctx.update as Update.CallbackQueryUpdate)?.callback_query) {
      ctx.session.user = (ctx.update as Update.CallbackQueryUpdate).callback_query.from;
    } else {
      console.error('User object could not be found in the context!');
    }
    await setFileLink(ctx);
    await setUserPhotoLink(ctx);
    await saveToDb(ctx.session);
    return await ctx.scene.leave();
  } else {
    ctx.session.userData.push({ question: {text:message, date_time:new Date()}, answer: MAGIC_ANSWER });
  }
  await ctx.reply(message, markup ?? { reply_markup: { remove_keyboard: true } });
  console.log(ctx.session.userData);
  return ctx.wizard.next();
}
`;
}

export let finish = `

bot.use(session());
bot.use(stage.middleware());

bot.launch();

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));`;

export function constructWizard(id: string, ARRAY_OF_FUNCTIONS: { name: string; code: string }[]): string {
  return `

const ${id} = new Scenes.WizardScene<MyContext>(
  \`${id}\`, ${ARRAY_OF_FUNCTIONS.map((f) => f.name).join(', ')}
);
${id}.command('/cancel', cancelConversation);
`;
}

export function constructStage(ARRAY_OF_WIZARD_NAMES: string[], CURRENT_WIZARD_NAME: string): string {
  return `

const stage = new Scenes.Stage<MyContext>(
  [${ARRAY_OF_WIZARD_NAMES.join(', ')}],
  {
    default: \`${CURRENT_WIZARD_NAME}\`,
  }
);`;
}

export function constructBranchHandler(NAME: string, ARRAY_OF_BUTTONS: { name: string; callback: string }[]): string {
  NAME += 'Handler';
  let actions: string[] = [];
  for (const button of ARRAY_OF_BUTTONS) {
    actions.push(`${NAME}.action(\'${button.callback}\', async (ctx) => {
      ctx.answerCbQuery();
  return ctx.scene.enter(\`${button.callback}Wizard\`, ctx.session.userData);
});`);
  }
  return `
const ${NAME} = new Composer<MyContext>();
${actions.join('\n')}
${NAME}.use((ctx) =>
  ctx.replyWithMarkdown("Please follow the last instruction")
);`;
}

export function constructHandler(NAME: string, type: 'photo' | 'location' | 'voice' | 'contact'): string {
  return `
const ${NAME} = new Composer<MyContext>();
${NAME}.on("${type}", async (ctx) => {
  console.log(ctx.message.${type});
  updateSession(ctx.session.userData, {${type}: ctx.message.${type}});
  const nextFunction = ctx.wizard.next().step as Function;
  ctx.session.isSpecial = true;
  return nextFunction(ctx);
});
${NAME}.use((ctx) =>
  ctx.replyWithMarkdown("Please reply with a \`${type}\`.")
);`;
}

export function constructAsyncRoot(MESSAGE: string, id: string) {
  return {
    name: id,
    code: `
const ${id} = async (ctx: MyContext) => {
  ctx.session.isSpecial = false;
  ctx.session.userData = [];
  ctx.session.userData.push({ question: {text:\`${MESSAGE}\`, date_time:new Date()}, answer: MAGIC_ANSWER });
  await ctx.reply(\`${MESSAGE}\`);
  return ctx.wizard.next();
};
`,
  };
}

export function constructAsyncFunction(
  MESSAGE: string,
  id: string,
  flag: boolean,
  ARRAY_OF_BUTTONS?: { name: string; callback: string }[],
  callback: string = ''
) {
  let buttons: string[] = [];
  if (ARRAY_OF_BUTTONS) {
    for (const button of ARRAY_OF_BUTTONS) {
      buttons.push(`Markup.button.callback(\`${button.name}\`, \`${button.callback}\`)`);
    }
  }

  let keyboard = buttons.length > 0 ? `, markup: Markup.inlineKeyboard([${buttons.join(', ')}])` : '';

  return {
    name: id,
    code: flag
      ? ``
      : `
const ${id} = async (ctx: MyContext) => {
  return await functionHandler({ ctx, message: \`${MESSAGE}\`${keyboard}});
};
`,
  };
}

export function constructAsyncFunctionReply(MESSAGE: string, id: string, flag: boolean, ARRAY_OF_BUTTONS: string[]) {
  let buttons: string[] = [];
  if (ARRAY_OF_BUTTONS) {
    for (const button of ARRAY_OF_BUTTONS) {
      buttons.push(`{ text: \`${button}\` }`);
    }
  }

  let keyboard = `, markup: { reply_markup: { keyboard: [[${buttons.join(
    ', '
  )}]], resize_keyboard: true, one_time_keyboard: true, remove_keyboard: true } }`;

  return {
    name: id,
    code: flag
      ? ``
      : `
const ${id} = async (ctx: MyContext) => {
  return await functionHandler({ ctx, message: \`${MESSAGE}\`${keyboard}});
};
`,
  };
}

export function constructAsyncEnd(MESSAGE: string, id: string, flag: boolean) {
  return {
    name: id,
    code: flag
      ? ``
      : `
const ${id} = async (ctx: MyContext) => {
  return await functionHandler({ ctx, message: \`${MESSAGE}\`, isEndNode: true });
};
`,
  };
}
