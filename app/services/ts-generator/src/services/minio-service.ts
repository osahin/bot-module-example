import { Client, ClientOptions } from 'minio';
import { EventEmitter } from 'stream';

export class MinioService extends EventEmitter {
  readonly path: string;
  minioClient: Client;
  bucketName: string;

  constructor(options: ClientOptions, bucketName: string, path: string) {
    super();
    this.bucketName = bucketName;
    this.path = path;
    this.minioClient = new Client(options);
  }

  upload(fileName: string) {
    const filePath = this.path + fileName;
    const metaData = {
      'Content-Type': 'application/octet-stream',
    };
    this.minioClient.bucketExists(this.bucketName, async (err: any, exist: boolean) => {
      if (exist) {
        console.log('Connected to the bucket.');
        this.minioClient.fPutObject(this.bucketName, fileName, filePath, metaData, (err: any) => {
          if (err) return console.error(err);
          console.log('File uploaded successfully.');
          this.emit('uploaded', fileName);
        });
      } else {
        console.error(`Bucket named ${this.bucketName} does not exist. It will be created.`);
        await this.minioClient.makeBucket(this.bucketName, '');
        this.minioClient.fPutObject(this.bucketName, fileName, filePath, metaData, (err: any) => {
          if (err) return console.error(err);
          console.log('File uploaded successfully.');
          this.emit('uploaded', fileName);
        });
      }
    });
  }
}
