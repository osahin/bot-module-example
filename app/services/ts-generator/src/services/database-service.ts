import mongoose, { Schema } from 'mongoose';

const Edges: Schema = new Schema({
  id: {
    type: String,
  },
  fromId: {
    type: String,
  },
  toId: {
    type: String,
  },
});
const Nodes: Schema = new Schema({
  id: {
    type: String,
  },
  node_text: {
    type: String,
  },
  type: {
    type: String,
  },
  message: {
    type: String,
  },
  keyboard: { type: [String] },
});

const DialogElement: Schema = new Schema({
  edges: [Edges],
  nodes: [Nodes],
});

const Connector: Schema = new Schema({
  beginConnectionPointIndex: {
    type: Number,
  },
  beginItemKey: {
    type: String,
  },
  endConnectionPointIndex: {
    type: Number,
  },
  endItemKey: {
    type: String,
  },
  points: [
    {
      x: {
        type: Number,
      },
      y: {
        type: Number,
      },
    },
  ],
  locked: {
    type: Boolean,
  },
  key: {
    type: String,
  },
  dataKey: {
    type: String,
  },
  zIndex: {
    type: Number,
  },
});
const Page: Schema = new Schema({
  height: {
    type: Number,
  },
  width: {
    type: Number,
  },
  pageColor: {
    type: Number,
  },
  pageHeight: {
    type: Number,
  },
  pageLandspace: {
    type: Boolean,
  },
  pageWidth: {
    type: Number,
  },
});
const Shape: Schema = new Schema({
  dataKey: {
    type: String,
  },
  height: {
    type: Number,
  },
  key: {
    type: String,
  },
  locked: {
    type: Boolean,
  },
  text: {
    type: String,
  },
  type: {
    type: String,
  },
  width: {
    type: Number,
  },
  x: {
    type: Number,
  },
  y: {
    type: Number,
  },
  zIndex: {
    type: Number,
  },
});
const Flow: Schema = new Schema({
  connectors: [Connector],
  page: Page,
  shapes: [Shape],
});

export interface IEdges {
  id: string;

  fromId: string;

  toId: string;
}

export interface INodes {
  id: string;

  node_text: string;

  type: string;

  message?: string;

  keyboard: string[];
}

export interface IDialogs {
  edges: Array<IEdges>;

  nodes: Array<INodes>;
}

export interface IBot extends Document {
  bot_id: number;

  bot_owner: string;

  bot_description: string;

  bot_name: string;

  token: string;

  dialogs: {
    dialog: IDialogs;
  };
}

export const BotSchema: Schema = new Schema<IBot>(
  {
    bot_id: {
      type: Number,
      required: true,
      unique: true,
    },
    bot_description: {
      type: String,
      required: true,
      unique: true,
    },
    bot_name: {
      type: String,
      required: true,
      unique: true,
    },
    token: {
      type: String,
      required: true,
      unique: true,
    },
    dialogs: {
      dialog: DialogElement,
      flow: Flow,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export async function findBotById(id: String) {
  let a = await mongoose.connect(
    `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/`,
    {
      socketTimeoutMS: 30000,
      keepAlive: true,
      autoIndex: false,
      retryWrites: false,
      dbName: 'BotModule',
    }
  );
  let BotModel = mongoose.model<IBot>('Bots', BotSchema);
  return BotModel.findById(id);
}

export let BOT = null;
