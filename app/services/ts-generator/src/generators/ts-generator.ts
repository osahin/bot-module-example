import { findBotById, IEdges, INodes } from '../services/database-service';
import fs from 'fs';
import {
  constructAsyncEnd,
  constructAsyncFunction,
  constructAsyncFunctionReply,
  constructAsyncRoot,
  constructBranchHandler,
  constructHandler,
  constructStage,
  constructWizard,
  finish,
  start,
} from '../templates/telegraf-bot-template';
import { MinioService } from '../services/minio-service';

interface Wizard {
  name: string;
  functions: { name: string; code: string }[];
}

export class TSGenerator {
  private minioService: MinioService;
  private readonly ROOT_MAGIC_ID;
  private botId: string;
  private botToken: string;
  private fileName: string;
  private filePath: string;

  private handlers: string[];
  private handlerIds: string[];
  private wizards: Wizard[];

  private nodes: INodes[];
  private edges: IEdges[];

  constructor(id: string, minioService: MinioService) {
    this.minioService = minioService;
    this.ROOT_MAGIC_ID = '_106';
    this.botId = id;
    this.botToken = '';
    this.fileName = `${this.botId}.ts`;
    this.filePath = this.minioService.path + this.fileName;
    this.handlers = [];
    this.handlerIds = [];
    this.wizards = [];
    this.nodes = [];
    this.edges = [];
  }

  async generate(): Promise<void> {
    await this.getBot();
    this.eventLoop(this.ROOT_MAGIC_ID, this.ROOT_MAGIC_ID + 'Wizard');
    this.writeToFile();
    await this.minioService.upload(this.fileName);
  }

  private writeToFile() {
    let functionString = '';
    for (const wizard of this.wizards) {
      for (const f of wizard.functions) {
        functionString += f.code;
      }
    }
    let wizardsString = '';
    const wizardNames: string[] = [];
    for (const wizard of this.wizards) {
      wizardsString += constructWizard(wizard.name, wizard.functions);
      wizardNames.push(wizard.name);
    }
    let stage = constructStage(wizardNames, wizardNames[0]);
    console.log('bot id', this.botId);
    if (!fs.existsSync(this.minioService.path)) {
      console.log(`creating ${this.minioService.path} dir`);
      fs.mkdirSync(this.minioService.path, { recursive: true });
    }
    fs.writeFileSync(this.filePath, start(this.botToken, this.botId));
    fs.appendFileSync(this.filePath, functionString);
    fs.appendFileSync(this.filePath, this.handlers.join('\n'));
    fs.appendFileSync(this.filePath, wizardsString);
    fs.appendFileSync(this.filePath, stage);
    fs.appendFileSync(this.filePath, finish);
  }

  private eventLoop(id: string, currentWizard: string) {
    let wizard = this.findWizardByName(currentWizard);
    if (!wizard) {
      this.wizards.push({ name: currentWizard, functions: [] });
    }
    let node = this.findNodeById(id);
    this.nodeHandler(node, currentWizard);
  }

  private async getBot() {
    const bot = await findBotById(this.botId);
    console.log(bot);
    if (bot && bot.dialogs.dialog.nodes && bot.dialogs.dialog.edges) {
      this.nodes = bot.dialogs.dialog.nodes;
      this.edges = bot.dialogs.dialog.edges;
      this.botToken = bot.token;
    } else {
      throw new Error('The bot could not be found or does not have any nodes/edges!');
    }

    for (let node of this.nodes) {
      node.id = '_' + node.id.split('-').join('');
    }

    for (let edge of this.edges) {
      edge.fromId = '_' + edge.fromId.split('-').join('');
      edge.toId = '_' + edge.toId.split('-').join('');
    }

    console.log(this.nodes);
  }

  private findNextNodes(id: string) {
    const nodes = [];
    for (const edge of this.edges) {
      if (edge.fromId == id) {
        nodes.push(edge.toId);
      }
    }
    return nodes;
  }

  private findNodeById(id: string) {
    const _nodes = this.nodes.find((el) => el.id == id);
    if (_nodes) {
      return _nodes;
    }
    throw new Error(`Could not find a node with the id: ${id}`);
  }

  private findWizardByName(name: string) {
    return this.wizards.find((el) => el.name == name);
  }

  private nodeHandler(node: INodes, currentWizard: string) {
    let wizard = this.findWizardByName(currentWizard)!;
    let wizardFinished = false;

    let flag = false;
    for (const wizard of this.wizards) {
      for (const func of wizard.functions) {
        if (func.name === node.id) {
          flag = true;
          break;
        }
      }
      if (flag) break;
    }

    let handlerFlag = false;
    for (const id of this.handlerIds) {
      if (id === node.id) {
        handlerFlag = true;
        break;
      }
    }

    switch (node.type) {
      // ADD ReplyKeyboard
      case 'root':
        wizard.functions.push(constructAsyncRoot(node.message!, node.id));
        break;
      case 'custom_text':
        wizard.functions.push(constructAsyncFunction(node.message!, node.id, flag));
        break;
      case 'reply_keyboard':
        if (node.keyboard.length < 1) {
          throw new Error('Reply keyboard has no buttons!');
        }
        wizard.functions.push(constructAsyncFunctionReply(node.message!, node.id, flag, node.keyboard));
        break;
      case 'multiple_choice':
        wizardFinished = true;
        let message = node.message!;
        let nextNodeIds = this.findNextNodes(node.id);
        let buttons = [];
        for (const nextNodeId of nextNodeIds) {
          let _node = this.findNodeById(nextNodeId)!;
          buttons.push({ name: _node.node_text, callback: _node.id });
        }
        wizard.functions.push(constructAsyncFunction(message, node.id, flag, buttons));
        if (!handlerFlag) {
          this.handlerIds.push(node.id);
          this.handlers.push(constructBranchHandler(node.id, buttons));
        }
        wizard.functions.push({ name: node.id + 'Handler', code: '' });
        for (const nextNodeId of nextNodeIds) {
          if (!flag && !handlerFlag) {
            this.eventLoop(this.findNextNodes(nextNodeId)[0], this.findNodeById(nextNodeId)!.id + 'Wizard');
          }
        }
        break;
      case 'custom_photo':
        wizard.functions.push(constructAsyncFunction(node.message!, node.id, flag));
        if (!handlerFlag) {
          this.handlerIds.push(node.id);
          this.handlers.push(constructHandler(node.id + 'Handler', 'photo'));
        }
        wizard.functions.push({ name: node.id + 'Handler', code: '' });
        break;
      case 'location':
        wizard.functions.push(constructAsyncFunction(node.message!, node.id, flag));
        if (!handlerFlag) {
          this.handlerIds.push(node.id);
          this.handlers.push(constructHandler(node.id + 'Handler', 'location'));
        }
        wizard.functions.push({ name: node.id + 'Handler', code: '' });
        break;
      case 'voice':
        wizard.functions.push(constructAsyncFunction(node.message!, node.id, flag));
        if (!handlerFlag) {
          this.handlerIds.push(node.id);
          this.handlers.push(constructHandler(node.id + 'Handler', 'voice'));
        }
        wizard.functions.push({ name: node.id + 'Handler', code: '' });
        break;
      case 'contact':
        wizard.functions.push(constructAsyncFunction(node.message!, node.id, flag));
        if (!handlerFlag) {
          this.handlerIds.push(node.id);
          this.handlers.push(constructHandler(node.id + 'Handler', 'contact'));
        }
        wizard.functions.push({ name: node.id + 'Handler', code: '' });
        break;
      case 'end':
        wizard.functions.push(constructAsyncEnd(node.message!, node.id, flag));
        break;
      default:
        console.log(node);
        throw new Error(`nodeType: ${node.type} could not be found in the switch statement!!!`);
    }

    if (!wizardFinished) {
      let nextNodeIds = this.findNextNodes(node.id);
      for (const nextNodeId of nextNodeIds) {
        if (!flag && !handlerFlag) {
          this.eventLoop(nextNodeId, currentWizard);
        }
      }
    }
  }
}
