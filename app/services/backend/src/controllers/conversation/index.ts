import { Request, Response } from 'express';
import { logger } from '../../services/logger-service';
import { ConversationModel } from '../../models/conversation';
import { conversationFileUpload } from '../../services/aws-service';
import { IConversation } from '../../types/conversation/conversation';
import { BadRequestError } from '../../errors/bad-request-error';

const createConversation = async (req: Request, res: Response) => {
  const bot: IConversation = req.body;
  const conversation = new ConversationModel(bot);
  await saveMinio(conversation);
  conversation
    .save()
    .then(() => {
      res.status(201).send(conversation);
    })
    .catch((error: any) => {
      res.status(500).send({ message: 'something went wrong when data insert to database', error: error });
    });
};

const getAllBotDataByToken = async (req: Request, res: Response) => {
  const { token } = req.params;
  const conversation = await ConversationModel.find({ token }).exec();
  res.status(200).json(conversation);
};

const clearBotDataByToken = async (req: Request, res: Response) => {
  const { token } = req.params;
  logger.info(`Clearing bot data for token: ${token}`);
  const result = await ConversationModel.deleteMany();
  res.status(200).send(result);
};

const saveMinio = async (data: IConversation) => {
  if (data.conversation.length > 100) {
    throw new BadRequestError('Conversation length must be less than 100');
  }
  const fileUpload: Promise<void>[] = [];
  for (const message of data.conversation) {
    if (message.answer.photo) {
      for (const photo of message.answer.photo) {
        if (photo.file_link) {
          fileUpload.push(conversationFileUpload(photo.file_link, `${data._id}/${photo.file_id}.jpg`));
          photo.file_link = `${process.env.ENDPOINT}/${process.env.CONVERSATION_BUCKET}/${data._id}/${photo.file_id}.jpg`;
        }
      }
    } else if (message.answer.voice) {
      fileUpload.push(
        conversationFileUpload(message.answer.voice.file_link, `${data._id}/${message.answer.voice.file_id}.jpg`)
      );
      message.answer.voice.file_link = `${process.env.ENDPOINT}/${process.env.CONVERSATION_BUCKET}/${data._id}/${message.answer.voice.file_id}.jpg`;
    }
  }
  await Promise.all(fileUpload);
};

export { createConversation, clearBotDataByToken, getAllBotDataByToken };
