import { Router } from 'express';
import { authRouter } from './auth';
import { companyRouter } from './company';
import { departmentRouter } from './department';
import { currentUser } from '../middlewares/current-user';
import { botRouter } from './bot';
import { conversationRouter } from './conversation';
import { requireAuth } from '../middlewares/require-auth';

const router: Router = Router();

router.use('/department', departmentRouter);

router.use('/company', companyRouter);

router.use('/auth', authRouter);

router.use('/bot', currentUser, requireAuth, botRouter);

router.use('/conversation', currentUser, conversationRouter);

export default router;
