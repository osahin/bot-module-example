import express from 'express';
import { body } from 'express-validator';
import { createConversation, clearBotDataByToken, getAllBotDataByToken } from '../../controllers/conversation';
import { validateRequest } from '../../middlewares/validate-request';
import { upload } from '../../services/aws-service';
import { requireAuth } from '../../middlewares/require-auth';

const conversationRouter = express.Router();
conversationRouter.post(
  '/create',
  [
    body('token').not().isEmpty().withMessage('Bot token is required'),
    body('conversation').not().isEmpty().withMessage('Conversation is required'),
    body('user').not().isEmpty().withMessage('User is required'),
  ],
  validateRequest,
  createConversation
);
conversationRouter.get('/:token', requireAuth, getAllBotDataByToken);
conversationRouter.delete('/', clearBotDataByToken);
export { conversationRouter };
