import { model, Schema } from 'mongoose';
import { ICompany } from '../types/company';

const companySchema = new Schema<ICompany>(
  {
    name: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Company = model<ICompany>('Company', companySchema);

export { Company };
