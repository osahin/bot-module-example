import { Schema } from 'mongoose';
import { IPage } from '../../types/bot/page';

export const Page = new Schema<IPage>({
  height: {
    type: Number,
  },
  width: {
    type: Number,
  },
  pageColor: {
    type: Number,
  },
  pageHeight: {
    type: Number,
  },
  pageLandspace: {
    type: Boolean,
  },
  pageWidth: {
    type: Number,
  },
});
