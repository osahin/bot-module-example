import { Schema } from 'mongoose';
import { INode } from '../../types/bot/node';

export const Node = new Schema<INode>({
  id: {
    type: String,
  },
  node_text: {
    type: String,
  },
  type: {
    type: String,
  },
  message: {
    type: String,
  },
  keyboard: {
    type: [String],
    required: false,
    unique: false,
  },
});
