import mongoose, { Schema, Document } from 'mongoose';
import { IConversation } from '../../types/conversation/conversation';
import { Message } from '../../types/conversation/message';

const ConversationSchema = new Schema(
  {
    token: { type: String, required: true },
    conversation: { type: Array<Message>, required: true },
    date_time: { type: Date, required: true },
    user: {
      type: {
        last_name: String,
        is_bot: Boolean,
        first_name: String,
        id: Number,
        username: String,
        language_code: String,
        photos: Array<String>,
      },
      required: true,
    },
  },
  { collection: 'bot_conversation' }
);

const ConversationModel = mongoose.model<IConversation>('bot_conversation', ConversationSchema);
export { ConversationModel };
