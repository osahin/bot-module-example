import { IConnector } from './connector';
import { IPage } from './page';
import { IShape } from './shape';

export interface IFlow extends Document {
  connectors: IConnector[];
  page: IPage[];
  shapes: IShape[];
}
