export interface IEdge {
  id: string;
  fromId: string;
  toId: string;
}
