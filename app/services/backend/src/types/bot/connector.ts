export interface IConnector extends Document {
  beginConnectionPointIndex: number;
  beginItemKey: string;
  endConnectionPointIndex: number;
  endItemKey: string;
  points: [
    {
      x: number;
      y: number;
    }
  ];
  locked: boolean;
  key: string;
  dataKey: string;
  zIndex: number;
}
