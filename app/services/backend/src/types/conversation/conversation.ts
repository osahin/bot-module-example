import { Message } from './message';
import { IUser } from './user';

export interface IConversation {
  _id?: string;
  token: string;
  conversation: Message[];
  date_time: Date;
  user: IUser;
}
