import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NavigationStart, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReplaySubject } from 'rxjs';
import { AppComponent } from './app.component';
import { PageLoaderComponent } from './layout/page-loader/page-loader.component';

const eventSubject = new ReplaySubject<RouterEvent>(1);
const routerMock = {
  navigate: jasmine.createSpy('navigate'),
  events: eventSubject.asObservable(),
  url: 'bots',
};
const windowMock = {
  location: {
    reload: () => {},
  },
};
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, BrowserAnimationsModule, NgxSpinnerModule],
      declarations: [AppComponent, PageLoaderComponent],
      providers: [
        { provide: Router, useValue: routerMock },
        { provide: Window, useValue: windowMock },
      ],
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    window.onbeforeunload = jasmine.createSpy();
    fixture.detectChanges();
  });
  it('should create the app', () => {
    expect(component).toBeTruthy();
  });
  it('should be spinner show', () => {
    spyOn(component, 'reload').and.callFake(() => {});
    component.currentUrl = '';
    eventSubject.next(new NavigationStart(0, routerMock.url));
    expect(component.currentUrl).not.toBe('');
  });
  it('should be spinner show', () => {
    component.currentUrl = '';
    eventSubject.next(new NavigationEnd(0, routerMock.url, routerMock.url));
    expect(component.currentUrl).toBe('');
  });
  it('should be location reload', () => {
    spyOnProperty(component, 'window', 'get').and.returnValue(windowMock);
    spyOn(component, 'reload').and.callThrough();
    component.reload();
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should be location reload', () => {
    spyOnProperty(component, 'window', 'get').and.returnValue(windowMock).and.callThrough();
    const compwindow = component.window;
    fixture.detectChanges();
    expect(compwindow).toBeTruthy();
  });
});
