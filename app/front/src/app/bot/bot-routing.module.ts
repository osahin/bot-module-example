import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatViewComponent } from './components/chat-view/chat-view.component';
import { BotComponent } from './components/bot/bot.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'list' },
  { path: 'add', component: BotComponent },
  { path: 'edit', component: BotComponent },
  { path: 'list', component: HomeComponent },
  { path: 'chat-view', component: ChatViewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BotRoutingModule {}
