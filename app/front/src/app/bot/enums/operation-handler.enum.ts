import { AddShape } from "../models/operations/add-shape";
import { ChangeConnectionUpdated } from "../models/operations/change-connection.updated";
import { DeleteShape } from "../models/operations/delete-shape.model";
import { ResizeCheck } from "../models/operations/resize-check.model";
import { ChangeConnectorPoints } from '../models/operations/change-connector-points.model';
import { BeforeChangeShapeText } from '../models/operations/before-change-shape-text.model';
import { ChangeShapeText } from '../models/operations/change-shape-text.model';

export const operationHandler = {
  addShape: AddShape,
  deleteShape: DeleteShape,
  resizeShape: ResizeCheck,
  changeConnection: ChangeConnectionUpdated,
  changeConnectorPoints: ChangeConnectorPoints,
  beforeChangeShapeText: BeforeChangeShapeText,
  changeShapeText: ChangeShapeText
};
