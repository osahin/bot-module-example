import { TextShape } from '../models/modal/text.model';
import { ReplyKeyboardShape } from '../models/modal/reply-keyboard.model';
import { RootShape } from '../models/modal/root.model';
import { PhotoShape } from '../models/modal/photo.model';
import { LocationShape } from '../models/modal/location.model';
import { VoiceShape } from '../models/modal/voice.model';
import { ContactShape } from '../models/modal/contact.model';
import { EndShape } from '../models/modal/end.model';
import { ChoiceShape } from '../models/modal/choice.model';
import { MultipleChoiceShape } from '../models/modal/multiple-choice.model';


export const modalHandler = {
    custom_text: TextShape,
    reply_keyboard: ReplyKeyboardShape,
    root:RootShape,
    custom_photo:PhotoShape,
    location:LocationShape,
    voice:VoiceShape,
    contact:ContactShape,
    choice:ChoiceShape,
    multiple_choice:MultipleChoiceShape,
    end:EndShape
  };