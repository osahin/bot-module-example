import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { GeneralNodeModalComponent } from './general-node-modal.component';

describe('GeneralNodeModalComponent', () => {
  let component: GeneralNodeModalComponent;
  let fixture: ComponentFixture<GeneralNodeModalComponent>;
  const dialogMock = {
    close: () => {},
    getState: () => 1,
  };
  const data = {
    message: 'unit-test-message',
    node_text: 'unit-test-node-text',
    type: 'custom_text',
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        BrowserModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
      ],
      declarations: [GeneralNodeModalComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
        {
          provide: MAT_DIALOG_DATA,
          useValue: data,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralNodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should create without message ', () => {
    delete component.data.message;
    spyOn(component, 'setFormValue').and.callThrough();
    component.setFormValue();
    expect(component).toBeTruthy();
    expect(component.setFormValue).toHaveBeenCalled();
    expect(component.messageForm.value.message).toBe('');
  });
  it('should close dialog', () => {
    spyOn(component, 'close').and.callThrough();
    component.close();
    expect(component.dialogRef.getState()).not.toEqual(0);
    expect(component.close).toHaveBeenCalled();
  });
  it('should save node object and close dialog', () => {
    spyOn(component, 'save').and.callThrough();
    component.save();
    expect(component.dialogRef.getState()).not.toEqual(0);
    expect(component.save).toHaveBeenCalled();
  });
});
