import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Node } from '../../models/bot/index';

@Component({
  selector: 'app-general-node-modal',
  templateUrl: './general-node-modal.component.html',
  styleUrls: ['./general-node-modal.component.scss'],
})
export class GeneralNodeModalComponent implements OnInit {
  answerType = '';

  constructor(
    public dialogRef: MatDialogRef<GeneralNodeModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Node,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.setFormValue();
  }

  setFormValue(): void {
    this.answerType = this.data.type;
    this.messageForm.setValue({
      message: this.data.message || '',
      name: this.data.node_text,
    });
  }

  save(): void {
    this.dialogRef.close({ node_text: this.messageForm.value.name, message: this.messageForm.value.message });
  }

  close(): void {
    this.dialogRef.close();
  }

  messageForm = this.formBuilder.group({
    message: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
  });

  errorMessages = {
    name: [{ type: 'required', message: 'Bu alanın doldurulması zorunludur...' }],
    message: [{ type: 'required', message: 'Bu alanın doldurulması zorunludur...' }],
  };
}
