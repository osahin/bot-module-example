import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ChatViewComponent } from './chat-view.component';
import { BotData } from '../../models/bot-data/bot-data.model';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BotService } from '@app/bot/services/bot.service';
import { of } from 'rxjs';

const botMockData: BotData[] = [
  {
    _id: '5f9f1b9b9b9b9b9b9b9b9b9b',
    token: '1234567890',
    conversation: [
      {
        answer: { date_time: new Date(), text: 'Hello' },
        question: { date_time: new Date(), text: 'Hi' },
      },
    ],
    date_time: new Date(),
    user: {
      id: 1234567890,
      is_bot: false,
      first_name: 'John',
      last_name: 'Doe',
      username: 'johndoe',
      language_code: 'en',
      photos: [],
    },
    photos: [],
  },
  {
    _id: '5f9f1b9b9b9b9b9b9b9b9b9b',
    token: '1234567890',
    conversation: [
      {
        answer: { date_time: new Date(), text: 'Hello' },
        question: { date_time: new Date(), text: 'Hi' },
      },
    ],
    date_time: new Date(),
    user: {
      id: 1234567890,
      is_bot: false,
      first_name: 'John',
      last_name: 'Doe',
      username: 'johndoe',
      language_code: 'en',
      photos: [],
    },
    photos: [],
  },
  {
    _id: '5f9f1b9b9b9b9b9b9b9b9b9b',
    token: '1234',
    conversation: [
      {
        answer: { date_time: new Date(), text: 'Hello' },
        question: { date_time: new Date(), text: 'Hi' },
      },
    ],
    date_time: new Date(),
    user: {
      id: 2,
      is_bot: false,
      first_name: 'John',
      last_name: 'Doe',
      username: 'johndoe',
      language_code: 'en',
      photos: [],
    },
    photos: [],
  },
];

describe('ChatViewComponent', () => {
  let component: ChatViewComponent;
  let fixture: ComponentFixture<ChatViewComponent>;
  let service: BotService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'bots/dashboard', component: ChatViewComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [ChatViewComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatViewComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(BotService);
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should load bot data', () => {
    spyOn(service, 'getBotData').and.returnValue(of(botMockData));
    spyOn(component, 'loadBotData').withArgs('1234567890').and.callThrough();
    spyOn(component, 'loadUserList').and.callThrough();
    component.loadBotData('1234567890');
    expect(component.loadBotData).toHaveBeenCalled();
    expect(component.botData).not.toHaveSize(0);
    expect(component.loadUserList).toHaveBeenCalled();
  });
  it('should routing to bot list', () => {
    spyOn(component, 'back').and.callThrough();
    component.back();
    fixture.detectChanges();
    expect(component.back).toHaveBeenCalled();
  });
  it('should load conversation by user id', () => {
    component.botData = [
      {
        _id: '',
        conversation: [
          { question: { text: 'sa', date_time: new Date() }, answer: { text: '', date_time: new Date() } },
        ],
        token: '',
        user: { id: 1, first_name: '', is_bot: false, last_name: '', username: '', language_code: '', photos: [] },
        photos: [],
        date_time: new Date(),
      },
    ];
    spyOn(component, 'selectUser')
      .withArgs({ id: 1, first_name: '', is_bot: false, last_name: '', username: '', language_code: '', photos: [] })
      .and.callThrough();
    component.selectUser({
      id: 1,
      first_name: '',
      is_bot: false,
      last_name: '',
      username: '',
      language_code: '',
      photos: [],
    });
    fixture.detectChanges();
    expect(component.selectUser).toHaveBeenCalled();
    expect(component.conversation).not.toHaveSize(0);
  });
});
