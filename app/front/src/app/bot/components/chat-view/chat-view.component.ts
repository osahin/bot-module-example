import { Component, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BotData, ChatWiewUser, TelegramUser } from '../../models/bot-data/index';
import { Message } from '../../models/bot-data/message.model';
import { BotService } from '../../services/bot.service';

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.component.html',
  styleUrls: ['./chat-view.component.scss'],
})
export class ChatViewComponent implements OnInit {
  hideRequiredControl = new UntypedFormControl(false);

  googleMapsLoaded: boolean;

  disabled = false;

  compact = false;

  invertX = false;

  invertY = false;

  shown: 'native' | 'hover' | 'always' = 'native';

  constructor(private activatedRouter: ActivatedRoute, private router: Router, private service: BotService) {}

  conversation: Array<Message> = [];

  botData: BotData[];

  userList: ChatWiewUser[] = [];

  selectedUser: TelegramUser = {
    first_name: '',
    id: 0,
    is_bot: false,
    language_code: '',
    last_name: '',
    username: '',
    photos: [],
  };

  ngOnInit(): void {
    this.activatedRouter.queryParams.subscribe(params => {
      this.loadBotData(params.token);
    });
  }

  loadBotData(token: string): void {
    this.service.getBotData(token).subscribe({
      next: (bots: BotData[]) => {
        this.botData = bots;
        this.loadUserList();
      },
      error: err => {
        // Add error handler
        console.error(err); // Log the error
      },
    });
  }

  loadUserList(): void {
    this.botData.forEach(data => {
      if (!this.userList.some(user => user.id === data.user.id)) {
        this.userList.push({ last_date: data.date_time.toString(), ...data.user });
      }
    });
  }

  selectUser(user: TelegramUser): void {
    this.conversation = this.botData.filter(data => data.user.id === user.id)[0].conversation;
    this.selectedUser = user;
  }

  isEndConversation = (message: Message): boolean => {
    return typeof message.answer !== 'string';
  };

  back(): void {
    this.router.navigate(['/bots/dashboard']);
  }
}
