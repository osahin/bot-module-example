import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Node } from '../../models/bot/node.model';

@Component({
  selector: 'app-reply-keyboard-modal',
  templateUrl: './reply-keyboard-modal.component.html',
  styleUrls: ['./reply-keyboard-modal.component.scss'],
})
export class ReplyKeyboardModalComponent implements OnInit {
  message: string;

  name: string;

  answerType = 'Text';

  optionList = [];

  node: Node;

  optionCount = 0;

  constructor(
    public dialogRef: MatDialogRef<ReplyKeyboardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Node,
    private formBuilder: UntypedFormBuilder
  ) {}

  ngOnInit(): void {
    this.setFormValue();
  }

  setFormValue(): void {
    this.node = { ...this.data };
    this.node.keyboard = this.data.keyboard || [];
    this.node.keyboard.forEach((option, index) => {
      const optionName = `option${index}`;
      this.errorMessages[`option${index}`] = [{ type: 'required', message: 'Bu alanın doldurulması zorunludur...' }];
      this.replyKeyboardForm.addControl(optionName, this.formBuilder.control(option, [Validators.required]));
    });
    this.replyKeyboardForm.patchValue({ message: this.data.message || '' });
    this.replyKeyboardForm.patchValue({ name: this.data.node_text });
  }

  save(): void {
    this.node.message = this.replyKeyboardForm.value.message;
    this.node.node_text = this.replyKeyboardForm.value.name;
    this.node.type = this.replyKeyboardForm.value.answerType;
    this.pushOption();
    this.dialogRef.close({ ...this.node });
  }

  close(): void {
    this.dialogRef.close();
  }

  pushOption(): void {
    Object.keys(this.replyKeyboardForm.value).forEach(key => {
      if (key.startsWith('option')) {
        this.optionList.push(this.replyKeyboardForm.value[key].toString());
      }
    });
    this.node.keyboard = this.optionList;
  }

  addOption(option: string, index: number): void {
    this.errorMessages[`option${index}`] = [{ type: 'required', message: 'Bu alanın doldurulması zorunludur...' }];
    this.replyKeyboardForm.addControl(`option${index}`, new UntypedFormControl(option, [Validators.required]));
    this.node.keyboard.push(option);
  }

  replyKeyboardForm = this.formBuilder.group({
    message: new UntypedFormControl('', [Validators.required]),
    name: new UntypedFormControl('', [Validators.required]),
  });

  errorMessages = {
    name: [{ type: 'required', message: 'Bu alanın doldurulması zorunludur...' }],
    message: [{ type: 'required', message: 'Bu alanın doldurulması zorunludur...' }],
  };
}
