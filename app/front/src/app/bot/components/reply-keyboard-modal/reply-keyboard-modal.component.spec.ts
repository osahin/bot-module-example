import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReplyKeyboardModalComponent } from './reply-keyboard-modal.component';

describe('ReplyKeyboardModalComponent', () => {
  let component: ReplyKeyboardModalComponent;
  let fixture: ComponentFixture<ReplyKeyboardModalComponent>;
  const dialogMock = {
    close: () => {},
    getState: () => 1,
  };
  const data = {
    message: 'unit-test-message',
    node_text: 'unit-test-node-text',
    type: 'replykeyboard',
    keyboard: ['asd', 'dsa'],
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, BrowserAnimationsModule, MatFormFieldModule, MatIconModule, MatInputModule],
      declarations: [ReplyKeyboardModalComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
        {
          provide: MAT_DIALOG_DATA,
          useValue: data,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplyKeyboardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should create without message', () => {
    delete data.message;
    spyOn(component, 'setFormValue').and.callThrough();
    component.setFormValue();
    expect(component).toBeTruthy();
    expect(component.replyKeyboardForm.value.message).toBe('');
  });
  it('should create without keyboards', () => {
    delete data.keyboard;
    spyOn(component, 'setFormValue').and.callThrough();
    component.setFormValue();
    expect(component).toBeTruthy();
    expect(component.node.keyboard).toHaveSize(0);
  });
  it('should add options message form', () => {
    const keyboarSize = component.node.keyboard.length;
    spyOn(component, 'addOption').withArgs('', 2).and.callThrough();
    component.addOption('', 2);
    expect(component.addOption).toHaveBeenCalled();
    expect(component.node.keyboard).toHaveSize(keyboarSize + 1);
  });
  it('should add options message form and save node', () => {
    const keyboarSize = component.node.keyboard.length;
    spyOn(component, 'addOption').withArgs('', 3).and.callThrough();
    component.addOption('', 3);
    expect(component.addOption).toHaveBeenCalled();
    expect(component.node.keyboard).toHaveSize(keyboarSize + 1);
    spyOn(component, 'save').and.callThrough();
    component.save();
    expect(component.save).toHaveBeenCalled();
    expect(component.node.keyboard).toHaveSize(keyboarSize + 1);
  });
  it('should close dialog', () => {
    spyOn(component, 'close').and.callThrough();
    component.close();
    expect(component.dialogRef.getState()).not.toEqual(0);
    expect(component.close).toHaveBeenCalled();
  });
  it('should save node object and close dialog', () => {
    spyOn(component, 'save').and.callThrough();
    component.save();
    expect(component.node).toBeTruthy();
    expect(component.dialogRef.getState()).not.toEqual(0);
    expect(component.save).toHaveBeenCalled();
  });
});
