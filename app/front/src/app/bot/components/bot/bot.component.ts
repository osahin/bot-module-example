import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { DxDiagramComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import ArrayStore from 'devextreme/data/array_store';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {
  RequestLayoutUpdateEvent,
  RequestEditOperationEvent,
  ItemClickEvent,
  CustomCommandEvent,
  dxDiagramChangeConnectionArgs,
} from 'devextreme/ui/diagram';
import { operationHandler } from '@app/bot/enums/operation-handler.enum';
import { Bot, BotConfig, Edge, Node } from '../../models/bot';
import { modalHandler } from '../../enums/modal-handler.enum';
import { BotService } from '../../services/bot.service';

@Component({
  selector: 'app-bot',
  templateUrl: './bot.component.html',
  styleUrls: ['./bot.component.scss'],
})
export class BotComponent implements OnInit, AfterViewInit {
  @ViewChild(DxDiagramComponent, { static: false }) diagram: DxDiagramComponent;

  botConfig: BotConfig = {
    name: 'New Bot',
    token: '',
    description: '',
  };

  tempBotConfig: BotConfig = {
    name: '',
    token: '',
    description: '',
  };

  nodes: Node[] = [{ id: '106', node_text: 'Start', type: 'root', message: 'Hi' }];

  edges: Edge[] = [];

  orgItemsDataSource: ArrayStore;

  flowEdgesDataSource: ArrayStore;

  nodesArr: Node[];

  edgesArr: Edge[];

  isConfigPanelVisible = false;

  bot: Bot;

  isEditPage: boolean;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private service: BotService
  ) {
    this.nodesArr = [];
    this.edgesArr = [];

    this.orgItemsDataSource = new ArrayStore({
      key: 'id',
      data: this.nodes,
    });
    this.flowEdgesDataSource = new ArrayStore({
      key: 'id',
      data: this.edges,
    });

    this.cancel = this.cancel.bind(this);
    this.save = this.save.bind(this);
  }

  ngOnInit(): void {
    if (this.router.url.includes('edit')) {
      this.isEditPage = true;
      this.activatedRouter.queryParams.subscribe(params => {
        this.getBotDetail(params.token);
      });
    }
  }

  ngAfterViewInit(): void {
    this.updateDiagram(this.bot);
  }

  getBotDetail(token: string): void {
    this.service.getBotByToken(token).subscribe({
      next: (data: Bot) => {
        if (data) {
          this.bot = { ...data };
          this.botConfig.description = data.description;
          this.botConfig.name = data.name;
          this.botConfig.token = data.token;
          this.tempBotConfig = this.botConfig;
          this.edges = [...this.bot.dialogs.dialog.edges];
          this.nodes = [...this.bot.dialogs.dialog.nodes];
          this.flowEdgesDataSource = new ArrayStore({
            key: 'id',
            data: this.edges,
          });
          this.orgItemsDataSource = new ArrayStore({
            key: 'id',
            data: this.nodes,
          });
        } else {
          this.showToastErr('Bot verileri alınamadı. Lütfen daha sonra tekrar deneyiniz.');
        }
      },
      error: () => {
        this.showToastErr('Bot verileri alınamadı. Lütfen daha sonra tekrar deneyiniz.');
      },
    });
  }

  updateDiagram(bot: Bot): void {
    if (bot && this.diagram) {
      this.diagram.instance.import(JSON.stringify(bot.dialogs.flow));
    }
  }

  cancel(): void {
    this.isConfigPanelVisible = false;
    this.tempBotConfig = this.botConfig;
  }

  save(): void {
    this.isConfigPanelVisible = false;
    // this.service.isBotExist(this.tempBotConfig.token).subscribe({
    //   next: (data: boolean) => {
    //     this.botConfig = this.tempBotConfig;
    //   },
    //   error: (err: any) => {
    //     this.showToastErr('Bot tokeni geçersiz. Lütfen tekrar deneyiniz.');
    //   },
    // });
    this.botConfig = this.tempBotConfig;
  }

  /**
   * Show toast of edit bot component for success
   */
  // eslint-disable-next-line class-methods-use-this
  showToast = (text: string): void => {
    notify({
      position: {
        at: 'top',
        my: 'top',
        of: '#diagram',
        offset: '0 4',
      },
      message: text,
      type: 'warning',
      delayTime: 1000,
    });
  };

  /**
   * Show toast of edit bot component for error
   */
  // eslint-disable-next-line class-methods-use-this
  showToastErr = (text: string): void => {
    notify({
      position: {
        at: 'center',
        my: 'center',
        of: '#diagram',
        offset: '0 4',
      },
      message: text,
      type: 'error',
      delayTime: 1000,
    });
  };

  /**
   * Determines whether custom command on
   * @param e
   */
  onCustomCommand(e: CustomCommandEvent): void {
    this.nodesArr = [];
    this.edgesArr = [];
    if (this.botConfig.token && this.botConfig.name && this.botConfig.description) {
      if (e.name === 'update') {
        this.updateBot();
      } else if (e.name === 'create') {
        this.nodes.forEach(x => this.nodesArr.push(x));
        this.edges.forEach(x => this.edgesArr.push(x));
        const data: Bot = {
          description: this.botConfig.description,
          name: this.botConfig.name,
          token: this.botConfig.token,
          dialogs: {
            dialog: {
              nodes: this.nodesArr.map(node => ({
                id: node.id,
                node_text: node.node_text,
                type: node.type,
                message: node.message,
                keyboard: node.keyboard,
              })),
              edges: this.edgesArr,
            },
            flow: JSON.parse(this.diagram.instance.export()),
          },
        };
        this.service.createBot(data).subscribe({
          next: state => {
            if (state) {
              this.showToast('Bot Eklendi');
              this.router.navigate(['/bots/list']);
            } else {
              this.showToastErr('Bot Eklenemedi !');
            }
          },
          error: () => {
            this.showToastErr(
              'Bot eklenemedi. Lütfen bot bilgilerinin doğru olduğundan emin olun veya daha sonra tekrar deneyiniz.'
            );
          },
        });
      } else if (e.name === 'setDetails') {
        this.isConfigPanelVisible = true;
      }
    } else if (e.name === 'setDetails') {
      this.isConfigPanelVisible = true;
    } else {
      this.showToastErr('Lütfen bot bilgilerini doldurunuz !');
    }
  }

  async updateBot(): Promise<void> {
    this.nodes.forEach(x => this.nodesArr.push(x));
    this.edges.forEach(x => this.edgesArr.push(x));
    const data: Bot = {
      // eslint-disable-next-line no-underscore-dangle
      _id: this.bot._id,
      id: this.bot.id,
      description: this.botConfig.description,
      name: this.botConfig.name,
      token: this.botConfig.token,
      dialogs: {
        dialog: {
          nodes: this.nodesArr,
          edges: this.edgesArr,
        },
        flow: JSON.parse(this.diagram.instance.export()),
      },
    };
    this.service.updateBot(data).subscribe({
      next: async response => {
        if (response) {
          setTimeout(() => {
            this.showToast('Bot başarıyla güncellendi. Ana sayfaya yönlendiriliyorsunuz.');
          }, 2000);
          await this.router.navigate(['/bots/list']);
        }
      },
      error: () => {
        this.showToastErr('Bot güncellenemedi. Lütfen daha sonra tekrar deneyiniz.');
      },
    });
  }

  createBot(data: Bot): boolean {
    let control = false;
    this.service.createBot(data).subscribe({
      next: state => {
        if (state) {
          control = true;
        }
      },
    });
    return control;
  }

  // eslint-disable-next-line class-methods-use-this
  requestLayoutUpdateHandler = (e: RequestLayoutUpdateEvent): void => {
    e.changes.forEach(change => {
      if (change.type === 'remove') {
        e.allowed = true;
      }
    });
  };

  requestEditOperationHandler(e: RequestEditOperationEvent): void {
    if (operationHandler[e.operation] && e.operation !== 'changeConnection') {
      e.allowed = operationHandler[e.operation].checkOperation(e.args);
    } else if (operationHandler[e.operation] && e.operation === 'changeConnection') {
      e.allowed = operationHandler[e.operation].checkOperation(
        e.args as dxDiagramChangeConnectionArgs,
        this.nodes,
        this.edges
      );
    } else {
      e.allowed = true;
    }
  }

  /**
   * Determines whether item click on
   * @param $event
   */
  onItemClick($event: ItemClickEvent): void {
    modalHandler[$event.item.dataItem.type].openDialog($event.item.dataItem, this.dialog).subscribe(result => {
      if (result) {
        this.orgItemsDataSource.push([{ type: 'update', key: $event.item.dataItem.id, data: result }]);
      }
    });
  }

  /**
   * Backs to bot list page
   */
  async back(): Promise<void> {
    await this.router.navigate(['/bots']);
  }
}
