import { ComponentFixture, fakeAsync, TestBed, flush } from '@angular/core/testing';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { ItemClickEvent, RequestEditOperationEvent } from 'devextreme/ui/diagram';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  DxButtonModule,
  DxDiagramModule,
  DxFormModule,
  DxPopupModule,
  DxScrollViewModule,
  DxTemplateModule,
  DxTextAreaModule,
  DxTextBoxModule,
  DxValidationSummaryModule,
  DxValidatorModule,
} from 'devextreme-angular';
import { MatTableModule } from '@angular/material/table';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BotService } from '@app/bot/services/bot.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { bot } from '@app/test/bot.mock';
import { modalHandler } from '@app/bot/enums/modal-handler.enum';
import { BotComponent } from './bot.component';
import { Bot } from '../../models/bot/bot.model';
import { SharedModule } from '../../../shared/shared.module';

const botMockData: Bot = bot;

const dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of({}), close: null });

describe('BotComponent', () => {
  let component: BotComponent;
  let fixture: ComponentFixture<BotComponent>;
  let service: BotService;
  let router: Router;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MatDialogModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          { path: 'bots', component: BotComponent },
          { path: 'bots/edit', component: BotComponent },
          { path: 'bots/add', component: BotComponent },
        ]),
        FormsModule,
        SharedModule,
        MatProgressSpinnerModule,
        DxDiagramModule,
        DxButtonModule,
        DxPopupModule,
        DxScrollViewModule,
        DxTemplateModule,
        DxFormModule,
        DxTextBoxModule,
        DxValidatorModule,
        DxValidationSummaryModule,
        DxTextAreaModule,
        MatTableModule,
        MatOptionModule,
        MatSelectModule,
        MatPaginatorModule,
        HttpClientTestingModule,
      ],
      declarations: [BotComponent],
      providers: [{ provide: ActivatedRoute, useValue: { queryParams: of({ id: '1' }) } }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BotComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(BotService);
    router = TestBed.inject(Router);
    router.navigateByUrl('bots/edit');
    spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    spyOnProperty(router, 'url', 'get').and.returnValue('/bots/edit');
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    spyOn(component, 'getBotDetail').withArgs('1').and.callThrough();
    component.getBotDetail('1');
    fixture.detectChanges();
    expect(component.getBotDetail).toHaveBeenCalled();
  });
  it('should show error if could not get bots', () => {
    spyOn(component, 'getBotDetail').withArgs('1').and.callThrough();
    component.getBotDetail('1');
    fixture.detectChanges();
    expect(component.getBotDetail).toHaveBeenCalled();
  });
  it('should request api if store is undefined', () => {
    component.bot = undefined;
    fixture.detectChanges();
    spyOn(component, 'getBotDetail').withArgs('1').and.callThrough();
    component.getBotDetail('1');
    fixture.detectChanges();
    expect(component.getBotDetail).toHaveBeenCalled();
    expect(component.bot).toBeFalsy();
  });
  it('should update dev extreme diagram', () => {
    component.bot = botMockData;
    fixture.detectChanges();
    spyOn(component, 'updateDiagram').withArgs(botMockData).and.callThrough();
    component.updateDiagram(botMockData);
    fixture.detectChanges();
    expect(component.updateDiagram).toHaveBeenCalled();
  });
  it('should show toast if error in selector', () => {
    component.bot = undefined;
    const spy = spyOn(service, 'getAllBots').and.throwError('error').and.callThrough();
    fixture.detectChanges();
    spyOn(component, 'getBotDetail').withArgs('1').and.callThrough();
    component.getBotDetail('1');
    fixture.detectChanges();
    expect(component.getBotDetail).toHaveBeenCalled();
    expect(component.bot).toBeFalsy();
    spy.calls.reset();
  });
  it('should routing to bot list', fakeAsync(() => {
    fixture.ngZone.run(() => {
      spyOn(component, 'back').and.callThrough();
      component.back();
      fixture.detectChanges();
      expect(component.back).toHaveBeenCalled();
    });
  }));
  it('should reset bot config if cancel popup', () => {
    component.tempBotConfig = { token: 'sss', description: '111', name: '222' };
    component.botConfig = { token: 'sss1', description: '1112', name: '2223' };
    spyOn(component, 'cancel').and.callThrough();
    component.cancel();
    fixture.detectChanges();
    expect(component.cancel).toHaveBeenCalled();
    expect(component.tempBotConfig).toEqual(component.botConfig);
  });
  it('should save bot config if save popup', () => {
    component.tempBotConfig = { token: 'sss', description: '111', name: '222' };
    component.botConfig = { token: 'sss1', description: '1112', name: '2223' };
    spyOn(component, 'save').and.callThrough();
    spyOn(service, 'isBotExist').withArgs(component.tempBotConfig.token).and.returnValue(of(true));
    component.save();
    fixture.detectChanges();
    expect(component.save).toHaveBeenCalled();
    expect(component.tempBotConfig).toEqual({
      token: component.botConfig.token,
      description: component.botConfig.description,
      name: component.botConfig.name,
    });
  });
  it('should nothing if event name is wrong', () => {
    spyOn(component, 'updateBot');
    spyOn(component, 'onCustomCommand')
      .withArgs({ component: undefined, element: undefined, name: 'test' })
      .and.callThrough();
    component.onCustomCommand({ component: undefined, element: undefined, name: 'test' });
    fixture.detectChanges();
    expect(component.onCustomCommand).toHaveBeenCalled();
    expect(component.isConfigPanelVisible).toBeFalse();
    expect(component.updateBot).not.toHaveBeenCalled();
  });
  it('should open popup dialog', () => {
    spyOn(component, 'onCustomCommand')
      .withArgs({ component: undefined, element: undefined, name: 'setDetails' })
      .and.callThrough();
    component.onCustomCommand({ component: undefined, element: undefined, name: 'setDetails' });
    fixture.detectChanges();
    expect(component.onCustomCommand).toHaveBeenCalled();
    expect(component.isConfigPanelVisible).toBeTrue();
  });
  it('should update bot', async () => {
    component.botConfig = { token: '123', description: '123', name: '123' };
    component.bot = botMockData;
    component.nodes = botMockData.dialogs.dialog.nodes;
    component.edges = botMockData.dialogs.dialog.edges;
    fixture.detectChanges();
    spyOn(component, 'onCustomCommand')
      .withArgs({ component: undefined, element: undefined, name: 'update' })
      .and.callThrough();
    component.onCustomCommand({ component: undefined, element: undefined, name: 'update' });
    fixture.detectChanges();
    expect(component.onCustomCommand).toHaveBeenCalled();
  });
  it("should show error if bot couldn't update", () => {
    spyOn(service, 'updateBot').and.returnValue(
      throwError(() => new Error(`'should show error if bot couldn't update' test thwrow error`))
    );
    const errorSpy = spyOn(component, 'showToastErr').and.callThrough();
    spyOn(component, 'updateBot').and.callThrough();
    component.botConfig = { token: '123', description: '123', name: '123' };
    component.bot = botMockData;
    component.nodes = botMockData.dialogs.dialog.nodes;
    component.edges = botMockData.dialogs.dialog.edges;
    fixture.detectChanges();
    spyOn(component, 'onCustomCommand')
      .withArgs({ component: undefined, element: undefined, name: 'update' })
      .and.callThrough();
    component.onCustomCommand({ component: undefined, element: undefined, name: 'update' });
    fixture.detectChanges();
    expect(component.onCustomCommand).toHaveBeenCalled();
    expect(errorSpy).toHaveBeenCalled();
    expect(component.updateBot).toHaveBeenCalled();
  });
  it('should error if create bot returned false', () => {
    component.botConfig.token = '123';
    component.botConfig.description = 'description';
    component.botConfig.name = 'name';
    component.nodes = botMockData.dialogs.dialog.nodes;
    component.edges = botMockData.dialogs.dialog.edges;
    fixture.detectChanges();
    spyOn(component, 'onCustomCommand')
      .withArgs({ component: undefined, element: undefined, name: 'create' })
      .and.callThrough();
    component.onCustomCommand({ component: undefined, element: undefined, name: 'create' });
    fixture.detectChanges();
    expect(component.onCustomCommand).toHaveBeenCalled();
  });
  it('should create bot', () => {
    component.botConfig.token = '123';
    component.botConfig.description = 'description';
    component.botConfig.name = 'name';
    component.nodes = botMockData.dialogs.dialog.nodes;
    component.edges = botMockData.dialogs.dialog.edges;
    fixture.detectChanges();
    spyOn(component, 'onCustomCommand')
      .withArgs({ component: undefined, element: undefined, name: 'create' })
      .and.callThrough();
    component.onCustomCommand({ component: undefined, element: undefined, name: 'create' });
    fixture.detectChanges();
    expect(component.onCustomCommand).toHaveBeenCalled();
  });
  it('should allow to remove when layout update', () => {
    const request = { allowed: false, component: undefined, changes: [{ type: 'remove' }], element: undefined };
    spyOn(component, 'requestLayoutUpdateHandler').withArgs(request).and.callThrough();
    component.requestLayoutUpdateHandler(request);
    expect(component.requestLayoutUpdateHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it("shouldn't allow to remove when layout update", () => {
    const request = {
      allowed: false,
      component: undefined,
      changes: [{ type: 'insert' }, { type: 'update' }],
      element: undefined,
    };
    spyOn(component, 'requestLayoutUpdateHandler').withArgs(request).and.callThrough();
    component.requestLayoutUpdateHandler(request);
    expect(component.requestLayoutUpdateHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow connect to root shape", () => {
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'root' }, connectorPosition: 'end' },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should allow if connect to root to unknown shape', () => {
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'root' }, connectorPosition: 'start' },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it("shouldn't allow add root shape", () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: 'addShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should allow if operation is undefined', () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: undefined,
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it("shouldn't allow connect from end shape", () => {
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'end' }, connectorPosition: 'start' },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow shape have 2 child", () => {
    component.edges = [
      { fromId: '1', toId: '2', id: '333' },
      { fromId: '1', toId: '3', id: '333' },
      { fromId: '1', toId: '4', id: '333' },
    ];
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'location' }, connectorPosition: 'start', connector: { fromKey: '1' } },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should be multiple choice have only choice type child', () => {
    component.nodes = [{ id: '1', type: 'location', node_text: 'multipleChoice', message: '' }];
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'multiple_choice' }, connectorPosition: 'start', connector: { toKey: '1' } },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should be choice have only multiple-choice type parent', () => {
    component.nodes = [{ id: '1', type: 'location', node_text: 'multipleChoice', message: '' }];
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'choice' }, connectorPosition: 'end', connector: { fromKey: '1' } },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow change root shape text", () => {
    const request: any = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: 'changeShapeText',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow change root shape text before", () => {
    const request: any = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: 'beforeChangeShapeText',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow if resize width to small", () => {
    const request: any = {
      args: { newSize: { width: 0.9, height: 2 } },
      component: undefined,
      element: undefined,
      operation: 'resizeShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow if resize height to small", () => {
    const request: any = {
      args: { newSize: { width: 1.5, height: 0.74 } },
      component: undefined,
      element: undefined,
      operation: 'resizeShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should open general node modal if click the custom_text shape', () => {
    fixture.detectChanges();
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'custom_text' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the custom_photo shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'custom_photo' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the reply_keyboard shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'reply_keyboard' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the choice shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'choice' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the multiple_choice shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'multiple_choice' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the location shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'location' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the contact shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'contact' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the voice shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'voice' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the end shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'end' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the root shape', () => {
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'root' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should allow to remove when layout update', () => {
    const request = { allowed: false, component: undefined, changes: [{ type: 'remove' }], element: undefined };
    spyOn(component, 'requestLayoutUpdateHandler').withArgs(request).and.callThrough();
    component.requestLayoutUpdateHandler(request);
    expect(component.requestLayoutUpdateHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it("shouldn't allow to remove when layout update", () => {
    const request = {
      allowed: false,
      component: undefined,
      changes: [{ type: 'insert' }, { type: 'update' }],
      element: undefined,
    };
    spyOn(component, 'requestLayoutUpdateHandler').withArgs(request).and.callThrough();
    component.requestLayoutUpdateHandler(request);
    expect(component.requestLayoutUpdateHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow connect to root shape", () => {
    const request: RequestEditOperationEvent = {
      args: { newShape: { type: 'root' }, connectorPosition: 'end' },
      component: undefined,
      element: undefined,
      operation: 'changeConnection',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it("shouldn't allow add root shape", () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: 'addShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should allow if operation is undefined', () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: undefined,
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it('should not allow delete root', () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { type: 'root' } },
      component: undefined,
      element: undefined,
      operation: 'deleteShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should not allow if shape have child', () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { attachedConnectorIds: ['1', '2'] } },
      component: undefined,
      element: undefined,
      operation: 'deleteShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should allow if shape have not child', () => {
    const request: RequestEditOperationEvent = {
      args: { shape: { attachedConnectorIds: [] } },
      component: undefined,
      element: undefined,
      operation: 'deleteShape',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it('should not allow connector have connectors connection more than 2', () => {
    const request: RequestEditOperationEvent = {
      args: {
        newPoints: [
          { x: 0, y: 0 },
          { x: 0, y: 0 },
          { x: 0, y: 0 },
        ],
      },
      component: undefined,
      element: undefined,
      operation: 'changeConnectorPoints',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should allow if connector have connectors connection count 2', () => {
    const request: RequestEditOperationEvent = {
      args: {
        newPoints: [
          { x: 0, y: 0 },
          { x: 0, y: 0 },
        ],
      },
      component: undefined,
      element: undefined,
      operation: 'changeConnectorPoints',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeTrue();
  });
  it('should not allow connector have connectors connection more than 2', () => {
    const request: RequestEditOperationEvent = {
      args: {
        newPoints: [
          { x: 0, y: 0 },
          { x: 0, y: 0 },
          { x: 0, y: 0 },
        ],
      },
      component: undefined,
      element: undefined,
      operation: 'changeConnectorPoints',
      reason: undefined,
      allowed: true,
      model: undefined,
    };
    spyOn(component, 'requestEditOperationHandler').withArgs(request).and.callThrough();
    component.requestEditOperationHandler(request);
    expect(component.requestEditOperationHandler).toHaveBeenCalled();
    expect(request.allowed).toBeFalse();
  });
  it('should open general node modal if click the custom_text shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'custom_text',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.custom_text, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'custom_text' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the custom_photo shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'custom_photo',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.custom_photo, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'custom_photo' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the reply_keyboard shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'reply_keyboard',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.reply_keyboard, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'reply_keyboard' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the choice shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'choice',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.choice, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'choice' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the multiple_choice shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'multiple_choice',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.multiple_choice, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'multiple_choice' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the location shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'location',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.location, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'location' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the contact shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'contact',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.contact, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'contact' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the voice shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'voice',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.voice, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'voice' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the end shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'end',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.end, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'end' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
  it('should open general node modal if click the root shape', () => {
    const shape = {
      id: '1',

      node_text: 'text',

      type: 'root',

      message: 'message',

      keyboard: ['keyboard'],
    };
    const spy = spyOn(modalHandler.root, 'openDialog').and.returnValue(of(shape));
    const request: ItemClickEvent = {
      component: undefined,
      element: undefined,
      item: { dataItem: { type: 'root' } },
    };
    spyOn(component, 'onItemClick').withArgs(request).and.callThrough();
    component.onItemClick(request);
    spy.and.callThrough().calls.reset();
    const callSpy = spy.and.callThrough();
    callSpy.calls.reset();
    fixture.detectChanges();
    expect(component.onItemClick).toHaveBeenCalled();
  });
});
