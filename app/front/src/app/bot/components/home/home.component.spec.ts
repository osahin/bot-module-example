import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { throwError, of } from 'rxjs';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { HomeComponent } from './home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BotService } from '@app/bot/services/bot.service';
import { Bot } from '../../models/bot/bot.model';
import { bot } from '@app/test/bot.mock';

const botMockData: Bot = bot;

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let service: BotService;
  const dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of({}), close: null });
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          { path: 'bots/edit', component: HomeComponent },
          { path: 'bots/add', component: HomeComponent },
          { path: 'bots/chat-view', component: HomeComponent },
        ]),
        BrowserAnimationsModule,
        MatTableModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
      ],
      declarations: [HomeComponent],
      providers: [provideMockStore()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    service = TestBed.inject(BotService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterAll(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should apply filter', () => {
    component.dataSource = new MatTableDataSource([]);
    const methods = spyOn(component, 'applyFilter').and.callThrough();
    const filterElement = fixture.debugElement.query(By.css('#filter')).nativeElement;
    filterElement.value = 'd';
    filterElement.dispatchEvent(
      new KeyboardEvent('keyup', { key: 'd', bubbles: true, cancelable: true, shiftKey: false })
    );
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(methods).toHaveBeenCalled();
  });
  it('should return bots fetch error', () => {
    spyOn(service, 'getAllBots').and.returnValue(
      throwError(() => new Error(`'should return bots fetch error' test throw error`))
    );
    const dialogSpy = spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    spyOn(component, 'getAllBots').and.callThrough();
    component.getAllBots();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(dialogSpy).toHaveBeenCalled();
  });
  it('should get all bots', () => {
    spyOn(service, 'getAllBots').and.returnValue(of([botMockData]));
    spyOn(component, 'getAllBots').and.callThrough();
    component.getAllBots();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.dataSource.data).toHaveSize(1);
    expect(component.bots).toHaveSize(1);
    expect(component.dataSource.data[0].name).toBe('bot1');
  });
  it('should routing to the add bot component', () => {
    spyOn(component, 'openAddBotModal').and.callThrough();
    component.openAddBotModal();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.openAddBotModal).toHaveBeenCalled();
  });
  it('should routing to the edit bot component with bot token parameter', () => {
    spyOn(component, 'openEditBotModal').and.callThrough();
    component.openEditBotModal('token');
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.openEditBotModal).toHaveBeenCalled();
  });
  it('should routing to the chat view component with bot token parameter', () => {
    spyOn(component, 'openChatView').and.callThrough();
    component.openChatView('token');
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.openChatView).toHaveBeenCalled();
  });
  it('bot should be deleted', () => {
    spyOn(service, 'deleteBotByToken').and.returnValue(of({ deletedBot: 'token' }));
    const dialogSpy = spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    fixture.detectChanges();
    spyOn(component, 'deleteBot').and.callThrough();
    component.deleteBot({
      description: 'description',
      name: 'name',
      token: 'token',
      _id: 'id',
      id: 2,
      dialogs: {
        dialog: { edges: [{ fromId: '', toId: '', id: '' }], nodes: [] },
        flow: {
          connectors: [],
          page: { height: 321, pageColor: 3, pageHeight: 2, pageLandspace: true, pageWidth: 1, width: 1 },
          shapes: [],
        },
      },
    });
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.deleteBot).toHaveBeenCalled();
    expect(dialogSpy).toHaveBeenCalled();
    // expect(component.getAllBots).toHaveBeenCalled();
  });
  it("bot shouldn't be deleted", () => {
    spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    fixture.detectChanges();
    spyOn(component, 'deleteBot').and.callThrough();
    component.deleteBot({
      description: 'description',
      name: 'name',
      token: 'token',
      _id: 'id',
      id: 2,
      dialogs: {
        dialog: { edges: [{ fromId: '', toId: '', id: '' }], nodes: [] },
        flow: {
          connectors: [],
          page: { height: 321, pageColor: 3, pageHeight: 2, pageLandspace: true, pageWidth: 1, width: 1 },
          shapes: [],
        },
      },
    });
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.deleteBot).toHaveBeenCalled();
  });
  it('should have a throw error if does not delete bot', () => {
    spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    spyOn(component, 'deleteBot').and.callThrough();
    component.deleteBot({
      description: 'description',
      name: 'name',
      token: 'token',
      _id: 'id',
      id: 2,
      dialogs: {
        dialog: { edges: [{ fromId: '', toId: '', id: '' }], nodes: [] },
        flow: {
          connectors: [],
          page: { height: 321, pageColor: 3, pageHeight: 2, pageLandspace: true, pageWidth: 1, width: 1 },
          shapes: [],
        },
      },
    });
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.deleteBot).toHaveBeenCalled();
  });
});
