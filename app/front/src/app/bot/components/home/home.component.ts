import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormControl } from '@angular/forms';
import { Bot } from '@app/bot/models/bot';
import { SuccessComponent } from '@app/shared/success/success.component';
import { ErrorComponent } from '@app/shared/error/error.component';
import { BotService } from '../../services/bot.service';
import { interval, mergeMap, startWith, Subscription } from 'rxjs';
import { registerLocaleData } from '@angular/common';
import localeTr from '@angular/common/locales/tr';
registerLocaleData(localeTr, 'tr');
@Component({
  selector: 'app-bot-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  filterText = '';

  isCompleted = false;

  bots: Array<Bot> = [];

  displayedColumns = ['name', 'description', 'createdAt', 'updatedAt', 'actions'];

  dataSource: MatTableDataSource<Bot>;

  statusObservable: Subscription;

  form = this.formBuilder.group({
    bot: new FormControl(''),
  });

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private formBuilder: FormBuilder,
    private service: BotService
  ) {}

  ngOnDestroy(): void {
    if (this.statusObservable) {
      this.statusObservable.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.form.setValue({
      bot: '',
    });
    this.getAllBots();
  }

  openAddBotModal(): void {
    this.router.navigateByUrl('/bots/add');
  }

  openEditBotModal(token: string): void {
    this.router.navigate(['/bots/edit'], { queryParams: { token: token } });
  }

  openChatView(token: string): void {
    this.router.navigate(['/bots/chat-view'], { queryParams: { token: token } });
  }

  deleteBot(bot: Bot): void {
    this.service.deleteBotByToken(bot).subscribe({
      next: response => {
        if (response.deletedBot) {
          this.getAllBots();
          this.dialog.open(SuccessComponent, {
            width: '440px',
            height: '280px',
            data: {
              buttonText: 'OK',
              title: 'İşlem Başarılı',
              message: `${bot.name} İsimli bot başarılı bir şekilde silinmiştir.`,
            },
          });
        } else {
          this.dialog.open(ErrorComponent, {
            width: '440px',
            height: '280px',
            data: {
              buttonText: 'OK',
              title: 'İşlem Başarısız',
              message: `${bot.name} İsimli bot silinemedi. Lütfen daha sonra tekrar deneyiniz.`,
            },
          });
        }
      },
      error: () => {
        this.dialog.open(ErrorComponent, {
          width: '440px',
          height: '280px',
          data: {
            buttonText: 'OK',
            title: 'İşlem Başarısız',
            message: `${bot.name} İsimli bot silinemedi. Lütfen daha sonra tekrar deneyiniz.`,
          },
        });
      },
    });
  }

  getAllBotStatus(): void {
    const botArray = this.bots.map(bot => {
      return { processId: `${bot._id}.ts`, status: false };
    });
    this.statusObservable = interval(10 * 1000)
      .pipe(
        startWith(0),
        mergeMap(() => this.service.getBotStatus(botArray))
      )
      .subscribe({
        next: response => {
          response.forEach(bot => {
            const index = this.bots.findIndex(b => `${b._id}.ts` === bot.processId);
            this.bots[index].status = bot.status;
          });
        },
        complete() {
          this.dataSource = new MatTableDataSource(this.bots);
        },
      });
    this.statusObservable;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllBots(): void {
    this.service.getAllBots().subscribe({
      next: response => {
        this.bots = response;
        this.dataSource = new MatTableDataSource(this.bots);
        this.isCompleted = true;
      },
      error: () => {
        this.isCompleted = true;
        this.dialog.open(ErrorComponent, {
          width: '440px',
          height: '280px',
          data: {
            buttonText: 'OK',
            title: 'İşlem Başarısız',
            message: 'Bot listesi alınamadı. Lütfen daha sonra tekrar deneyiniz.',
          },
        });
      },
      complete: () => {
        this.getAllBotStatus();
      },
    });
  }

  stopBot(id: string): void {
    this.service.stopBot(id).subscribe({
      next: response => {
        if (response) {
          this.dialog.open(SuccessComponent, {
            width: '440px',
            height: '280px',
            data: {
              buttonText: 'OK',
              title: 'İşlem Başarılı',
              message: 'Bota durdurulmasi için sinyal gönderildi. Bir kaç saniye içinde durmuş olacaktır.',
            },
          });
        } else {
          this.dialog.open(ErrorComponent, {
            width: '440px',
            height: '280px',
            data: {
              buttonText: 'OK',
              title: 'İşlem Başarısız',
              message: 'Bot durdurulamadi. Lütfen daha sonra tekrar deneyiniz.',
            },
          });
        }
      },
      error: () => {
        this.dialog.open(ErrorComponent, {
          width: '440px',
          height: '280px',
          data: {
            buttonText: 'OK',
            title: 'İşlem Başarısız',
            message: 'Bot durdurulamadi. Lütfen daha sonra tekrar deneyiniz.',
          },
        });
      },
    });
  }

  startBot(id: string): void {
    this.service.startBot(id).subscribe({
      next: response => {
        if (response) {
          this.dialog.open(SuccessComponent, {
            width: '440px',
            height: '280px',
            data: {
              buttonText: 'OK',
              title: 'İşlem Başarılı',
              message: 'Bota başlaması için sinyal gönderildi. Bir kaç saniye içinde başlamış olacaktır.',
            },
          });
        } else {
          this.dialog.open(ErrorComponent, {
            width: '440px',
            height: '280px',
            data: {
              buttonText: 'OK',
              title: 'İşlem Başarısız',
              message: 'Bot başlatılamadı. Lütfen daha sonra tekrar deneyiniz.',
            },
          });
        }
      },
      error: () => {
        this.dialog.open(ErrorComponent, {
          width: '440px',
          height: '280px',
          data: {
            buttonText: 'OK',
            title: 'İşlem Başarısız',
            message: 'Bot başlatılamadı. Lütfen daha sonra tekrar deneyiniz.',
          },
        });
      },
    });
  }
}
