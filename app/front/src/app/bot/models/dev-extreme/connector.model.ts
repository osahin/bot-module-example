import { Point } from "./point.model";

export class Connector {
    beginConnectionPointIndex: number;

    beginItemKey: string;

    endConnectionPointIndex: number;

    endItemKey: string;

    points: Point[];

    locked: boolean;

    key: string;

    dataKey: string;

    zIndex: number;
}