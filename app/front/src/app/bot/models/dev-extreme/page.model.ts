export class Page {
    height: number;

    width: number;

    pageColor: number;

    pageHeight: number;

    pageLandspace: boolean;

    pageWidth: number;
}