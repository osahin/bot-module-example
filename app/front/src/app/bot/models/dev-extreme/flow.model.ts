import { Connector } from "./connector.model";
import { Page } from "./page.model";
import { Shape } from "./shape.model";

export class Flow{
    connectors:Connector[];

    page:Page;

    shapes:Shape[];
}
