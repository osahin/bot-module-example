
export class Shape{
    dataKey:string;

    height:number;

    key:string;

    locked:boolean;

    text:string;

    type:string;

    width:number;

    x:number;

    y:number;

    zIndex:number;
}
