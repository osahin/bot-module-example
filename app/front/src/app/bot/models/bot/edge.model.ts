
export class Edge {
    id: string | number;

    fromId: string | number;

    toId: string | number;

    text?: string;
}