export class BotConfig {
  id?: number;

  token: string;

  name: string;

  description: string;
}
