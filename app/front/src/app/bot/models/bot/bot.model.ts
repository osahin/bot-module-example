import { BotDetail } from './dialog.model';

export class Bot {
  _id?: string;

  id?: number;

  token: string;

  status?: boolean;

  name: string;

  description: string;

  dialogs: BotDetail;

  createdAt?: Date;

  updatedAt?: Date;
}
