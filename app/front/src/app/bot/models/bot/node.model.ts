export class Node {
  id: string;

  node_text: string;

  type: string;

  message: string;

  keyboard?: string[];
}
