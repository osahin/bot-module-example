import { Flow } from '../dev-extreme/flow.model';
import { BotDialog } from './bot-dialog';

export class BotDetail {
  dialog: BotDialog;

  flow: Flow;
}
