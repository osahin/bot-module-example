import { dxDiagramChangeConnectorPointsArgs } from 'devextreme/ui/diagram';

export class ChangeConnectorPoints {

    static checkOperation(args: dxDiagramChangeConnectorPointsArgs): boolean {
      if (args.newPoints.length > 2) {
        return false;
      }
      return true;
    }
  }