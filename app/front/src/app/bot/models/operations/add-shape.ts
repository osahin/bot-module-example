import { dxDiagramAddShapeArgs } from 'devextreme/ui/diagram';

export class AddShape {

  static checkOperation(args: dxDiagramAddShapeArgs): boolean {
    return args.shape.type !== 'root';
  }
}
