import { dxDiagramChangeShapeTextArgs } from 'devextreme/ui/diagram';

export class ChangeShapeText {
  static checkOperation = (args: dxDiagramChangeShapeTextArgs): boolean => args.shape.type !== 'root';
}
