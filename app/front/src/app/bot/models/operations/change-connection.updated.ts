import { dxDiagramChangeConnectionArgs } from 'devextreme/ui/diagram';
import { Edge } from '../bot/edge.model';
import { Node } from '../bot/node.model';

export class ChangeConnectionUpdated {
  static checkOperation(args: dxDiagramChangeConnectionArgs, nodes: Node[], edges: Edge[]): boolean {
    const shapetype = args.newShape && args.newShape.type;

    // roota giren edge olamaz
    if (shapetype === 'root' && args.connectorPosition === 'end') {
      return false;
    }
    // endden çıkan edge olamaz
    if (shapetype === 'end' && args.connectorPosition === 'start') {
      return false;
    }
    // connector yoksa from key undefined gelir
    if (args.connector) {
      // multiple choice dışında birden fazla edge çıkamaz
      if (
        args.connectorPosition === 'start' &&
        shapetype !== 'multiple_choice' &&
        this.haveAChild(args.connector.fromKey, edges)
      ) {
        return false;
      }
      // multiple choiceun child nodeları choice tipinde olmalı
      if (
        args.connectorPosition === 'start' &&
        shapetype === 'multiple_choice' &&
        !this.checkNodeType(args.connector.toKey, nodes, 'choice')
      ) {
        return false;
      }
      // choiceun parent nodeları multiple choice tipinde olmalı
      if (
        shapetype === 'choice' &&
        args.connectorPosition === 'end' &&
        !this.checkNodeType(args.connector.fromKey, nodes, 'multiple_choice')
      ) {
        return false;
      }
    }
    return true;
  }

  /**
   * verilen noden id'den çıkan edge olup olmadığını kontrol eder
   * @param id string - node id
   * @param edges
   * @returns true if a have child
   */
  static haveAChild(id: string, edges: Edge[]): boolean {
    let childs: Edge[] = [];
    childs = edges.filter(e => e.fromId === id);
    return childs.length > 0;
  }

  /**
   * Checks node type
   * @param id
   * @param nodes
   * @param type
   * @returns true if node type
   */
  static checkNodeType(id: string, nodes: Node[], type: 'choice' | 'multiple_choice'): boolean {
    return nodes.find(e => e.id === id).type === type;
  }
}
