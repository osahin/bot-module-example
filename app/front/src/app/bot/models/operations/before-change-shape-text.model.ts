import { dxDiagramBeforeChangeShapeTextArgs } from 'devextreme/ui/diagram';

export class BeforeChangeShapeText {
  static checkOperation = (args: dxDiagramBeforeChangeShapeTextArgs): boolean => args.shape.type !== 'root';
}
