import { dxDiagramDeleteShapeArgs } from 'devextreme/ui/diagram';

export class DeleteShape {
  static checkOperation(args: dxDiagramDeleteShapeArgs): boolean {
    if (args.shape.type === 'root') {
      return false;
    }

    if (args.shape.attachedConnectorIds.length > 0) {
      return false;
    }

    return true;
  }
}
