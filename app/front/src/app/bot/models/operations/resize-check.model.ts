import { dxDiagramResizeShapeArgs } from 'devextreme/ui/diagram';

export class ResizeCheck {
  static checkOperation = (args: dxDiagramResizeShapeArgs): boolean =>
    !(args.newSize.width < 1 || args.newSize.height < 0.75);
}
