import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ReplyKeyboardModalComponent } from '../../components/reply-keyboard-modal/reply-keyboard-modal.component';
import { Node } from '../bot/node.model';

export class ReplyKeyboardShape {
  static openDialog(shape: Node, dialog: MatDialog): Observable<Record<string, unknown>> {
    return dialog
      .open(ReplyKeyboardModalComponent, {
        width: '600px',
        height: '500px',
        data: shape,
      })
      .afterClosed();
  }
}
