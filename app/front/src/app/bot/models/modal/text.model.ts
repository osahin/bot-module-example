import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { GeneralNodeModalComponent } from '../../components/general-node-modal/general-node-modal.component';
import { Node } from '../bot/node.model';

export class TextShape {
  static openDialog(shape: Node, dialog: MatDialog): Observable<Record<string, unknown>> {
    return dialog
      .open(GeneralNodeModalComponent, {
        width: '600px',
        height: '300px',
        data: shape,
      })
      .afterClosed();
  }
}
