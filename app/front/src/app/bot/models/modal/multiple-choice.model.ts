import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { GeneralNodeModalComponent } from '../../components/general-node-modal/general-node-modal.component';
import { Node } from '../bot/node.model';

export class MultipleChoiceShape {
  static openDialog(shape: Node, dialog: MatDialog): Observable<Record<string, unknown>> {
    return dialog
      .open(GeneralNodeModalComponent, {
        width: '600px',
        height: '300px',
        data: shape,
      })
      .afterClosed();
  }
}
