import { Message } from './message.model';
import { TelegramUser } from './telegram-user.model';

export class BotData {
  _id: string;

  token: string;

  conversation: Message[];

  date_time: Date;

  user: TelegramUser;

  photos: string[];
}
