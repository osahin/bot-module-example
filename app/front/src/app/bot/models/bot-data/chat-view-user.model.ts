import { User } from './user.interface';

export class ChatWiewUser implements User {
  id: number;

  first_name: string;

  last_name?: string;

  is_bot: boolean;

  language_code: string;

  last_date: string;

  photos?: string[];
}
