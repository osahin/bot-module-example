import { User } from './user.interface';

export class TelegramUser implements User {
  id: number;

  username: string;

  first_name: string;

  last_name?: string;

  is_bot: boolean;

  language_code: string;

  photos: string[];
}
