import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BotService } from './bot.service';
import { Bot } from '../models/bot';

const botMockData: Bot = {
  _id: '1',
  id: 21,
  description: 'Description for unit test',
  name: 'Bot name for unit test',
  token: 'DSLKFSŞLDKFSDLFKSDŞLFKLŞDSKFLDSŞKF',
  dialogs: {
    dialog: { edges: [{ id: '', fromId: '', toId: '' }], nodes: [{ id: '', node_text: '', type: '', message: '' }] },
    flow: {
      connectors: [
        {
          beginConnectionPointIndex: 0,
          beginItemKey: '',
          endConnectionPointIndex: 0,
          endItemKey: '',
          points: [{ x: 0, y: 0 }],
          locked: false,
          key: '',
          dataKey: '',
          zIndex: 0,
        },
      ],
      page: { height: 1, width: 1, pageColor: 3, pageHeight: 1, pageLandspace: false, pageWidth: 3 },
      shapes: [
        {
          dataKey: 'asd',
          key: 'asd',
          type: 'asd',
          x: 1,
          y: 1,
          width: 200,
          height: 200,
          text: '',
          zIndex: 1,
          locked: false,
        },
      ],
    },
  },
};

describe('BotService', () => {
  let service: BotService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(BotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be called getAllBots', async () => {
    service.getAllBots().subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be called getBotById', async () => {
    service.getBotByToken('DSLKFSŞLDKFSDLFKSDŞLFKLŞDSKFLDSŞKF').subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be called deleteBotById', async () => {
    service.getBotByToken('DSLKFSŞLDKFSDLFKSDŞLFKLŞDSKFLDSŞKF').subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be called updateBot', async () => {
    service.updateBot(botMockData).subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be called createBot', async () => {
    service.createBot(botMockData).subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be called getBotData', async () => {
    service.getBotData('123').subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be start bot', () => {
    service.startBot('123').subscribe({
      next(value) {
        expect(value).toBeTruthy();
      },
    });
  });
  it('should be stop bot', () => {
    service.stopBot('123').subscribe({
      next(value) {
        expect(value).toBeTruthy();
      },
    });
  });
  it('bot if exist', () => {
    service.isBotExist('123').subscribe({
      next(value) {
        expect(value).toBeTruthy();
      },
    });
  });
});
