import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BotData } from '../models/bot-data/bot-data.model';
import { Bot, Edge, Node } from '../models/bot/index';

@Injectable({
  providedIn: 'root',
})
export class BotService {
  nodeArray: Node[];

  edges: Edge[] = [];

  constructor(private http: HttpClient) {}

  getAllBots(): Observable<Bot[]> {
    return this.http.get<Array<Bot>>('~api/bot/get-all-bots');
  }

  getBotByToken(token: string): Observable<Bot> {
    return this.http.get<Bot>(`~api/bot/get-bot-by-token/${token}`);
  }

  deleteBotByToken(bot: Bot): Observable<{ deletedBot: string }> {
    return this.http.delete<{ deletedBot: string }>(`~api/bot/${bot.token}`);
  }

  updateBot(bot: Bot): Observable<Bot> {
    return this.http.put<Bot>('~api/bot/update', bot);
  }

  createBot(bot: Bot): Observable<Bot> {
    return this.http.post<Bot>('~api/bot/create', bot);
  }

  startBot(id: string): Observable<{ result: string }> {
    return this.http.get<{ result: string }>(`~processrunner/start-bot/${id}.ts`);
  }

  stopBot(id: string): Observable<{ result: string }> {
    return this.http.get<{ result: string }>(`~processrunner/stop-bot/${id}.ts`);
  }

  getBotStatus(
    botArray: Array<{ processId: string; status: boolean }>
  ): Observable<Array<{ processId: string; status: boolean }>> {
    return this.http.post<Array<{ processId: string; status: boolean }>>(
      `~processrunner/states-of-processes`,
      botArray
    );
  }

  getBotData(token: string): Observable<BotData[]> {
    return this.http.get<BotData[]>(`~api/conversation/${token}`);
  }
}
