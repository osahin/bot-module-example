import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import {
  DxDiagramModule,
  DxButtonModule,
  DxPopupModule,
  DxScrollViewModule,
  DxTemplateModule,
  DxFormModule,
  DxTextBoxModule,
  DxValidatorModule,
  DxValidationSummaryModule,
  DxTextAreaModule,
} from 'devextreme-angular';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ChatViewComponent } from './components/chat-view/chat-view.component';
import { HomeComponent } from './components/home/home.component';
import { GeneralNodeModalComponent } from './components/general-node-modal/general-node-modal.component';
import { ReplyKeyboardModalComponent } from './components/reply-keyboard-modal/reply-keyboard-modal.component';
import { BotRoutingModule } from './bot-routing.module';
import { BotComponent } from './components/bot/bot.component';
import { SharedModule } from '../shared/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatMenuModule } from '@angular/material/menu';
import { GoogleMapsModule } from '@angular/google-maps';

export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, 'i18n/', '.json');
}
@NgModule({
  declarations: [
    BotComponent,
    HomeComponent,
    ChatViewComponent,
    GeneralNodeModalComponent,
    ReplyKeyboardModalComponent,
  ],
  imports: [
    CommonModule,
    BotRoutingModule,
    FormsModule,
    SharedModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    DxDiagramModule,
    DxButtonModule,
    DxPopupModule,
    DxScrollViewModule,
    DxTemplateModule,
    DxFormModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxValidationSummaryModule,
    DxTextAreaModule,
    MatTableModule,
    MatOptionModule,
    MatSelectModule,
    MatPaginatorModule,
    MatMenuModule,
    GoogleMapsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
})
export class BotModule {}
