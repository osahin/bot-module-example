import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RightSidebarService } from 'src/app/core/service/rightsidebar.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { ConfigService } from '@app/config/config.service';
import { MatIconModule } from '@angular/material/icon';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let service: ConfigService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{ path: 'authentication/signin', component: HeaderComponent }]),
        TranslateModule.forRoot(),
        MatIconModule,
        NgbModule,
      ],
      declarations: [HeaderComponent],
      providers: [RightSidebarService, TranslateService, ConfigService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = TestBed.inject(ConfigService);
  });

  it('should create', () => {
    localStorage.removeItem('theme');
    localStorage.removeItem('menuOption');
    localStorage.removeItem('choose_logoheader');
    localStorage.removeItem('sidebar_status');
    localStorage.removeItem('lang');
    expect(component).toBeTruthy();
  });
  it('should create', () => {
    localStorage.removeItem('theme');
    localStorage.removeItem('menuOption');
    localStorage.removeItem('choose_logoheader');
    localStorage.removeItem('sidebar_status');
    const serviceSpy = spyOnProperty(service, 'config', 'get').and.returnValue({
      layout: {
        rtl: false, // options:  true & false
        variant: 'light', // options:  light & dark
        themeColor: 'white', // options:  white, black, purple, blue, cyan, green, orange
        logoBgColor: 'white', // options:  white, black, purple, blue, cyan, green, orange
        sidebar: {
          collapsed: true, // options:  true & false
          backgroundColor: 'light', // options:  light & dark
        },
      },
    });
    fixture.detectChanges();
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    spyOn(component, 'ngAfterViewInit').and.callThrough();
    component.ngAfterViewInit();
    fixture.detectChanges();
    localStorage.removeItem('lang');
    component.flagvalue = 'assets/images/flags/turkey.png';
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.flagvalue).toEqual('assets/images/flags/turkey.png');
    expect(component.flagvalue).not.toEqual('assets/images/flags/us.jpg');
    serviceSpy.calls.reset();
  });
  it('should create with local storage item', () => {
    localStorage.setItem('theme', 'dark');
    localStorage.setItem('menuOption', 'mini');
    localStorage.setItem('choose_logoheader', 'skin1');
    localStorage.setItem('sidebar_status', 'close');
    localStorage.setItem('lang', 'fr');
    component.flagvalue = null;
    spyOn(component, 'ngAfterViewInit').and.callThrough();
    component.ngAfterViewInit();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    localStorage.setItem('lang', 'en');
  });
  it('should create with local storage item and flag value undefined', () => {
    localStorage.setItem('theme', 'dark');
    localStorage.setItem('menuOption', 'mini');
    localStorage.setItem('choose_logoheader', 'skin1');
    localStorage.setItem('sidebar_status', 'open');
    component.flagvalue = 'assets/images/flags/turkey.png';
    localStorage.setItem('lang', 'fr');
    expect(component).toBeTruthy();
    localStorage.setItem('lang', 'en');
  });
  it('should create with open side bar', () => {
    localStorage.setItem('sidebar_status', 'open');
    expect(component).toBeTruthy();
  });
  it('should create with config side bar collapse', () => {
    localStorage.removeItem('theme');
    localStorage.removeItem('menuOption');
    localStorage.removeItem('choose_logoheader');
    localStorage.removeItem('sidebar_status');
    component.config.layout.sidebar.collapsed = true;
    expect(component).toBeTruthy();
  });
  it('should set language', () => {
    spyOn(component, 'setLanguage').withArgs('en', 'en', 'en').and.callThrough();
    component.setLanguage('en', 'en', 'en');
    expect(component.setLanguage).toHaveBeenCalled();
    expect(component.setLanguage).toHaveBeenCalledWith('en', 'en', 'en');
    expect(component.countryName).toBe('en');
    expect(component.flagvalue).toBe('en');
    expect(component.langStoreValue).toBe('en');
  });
  it('should side menu expand', () => {
    spyOn(component, 'callSidemenuCollapse').and.callThrough();
    component.callSidemenuCollapse();
    expect(component.callSidemenuCollapse).toHaveBeenCalled();
  });
  it('should side menu collapse', () => {
    spyOn(component, 'callSidemenuCollapse').and.callThrough();
    component.callSidemenuCollapse();
    component.callSidemenuCollapse();
    expect(component.callSidemenuCollapse).toHaveBeenCalledTimes(2);
  });
  it('should logout', () => {
    spyOn(component, 'logout').and.callThrough();
    component.logout();
    expect(component.logout).toHaveBeenCalled();
  });
  it('should toogle right side bar', () => {
    spyOn(component, 'toggleRightSidebar').and.callThrough();
    component.toggleRightSidebar();
    expect(component.toggleRightSidebar).toHaveBeenCalled();
  });
  it('should open mobile level 1 menu', () => {
    spyOn(component, 'mobileMenuSidebarOpen')
      .withArgs(
        {
          target: {
            classList: document.querySelector('a').classList,
          },
        },
        '1'
      )
      .and.callThrough();
    component.mobileMenuSidebarOpen(
      {
        target: {
          classList: document.querySelector('a').classList,
        },
      },
      '1'
    );
    expect(component.mobileMenuSidebarOpen).toHaveBeenCalled();
    expect(component.mobileMenuSidebarOpen).toHaveBeenCalledWith(
      {
        target: {
          classList: document.querySelector('a').classList,
        },
      },
      '1'
    );
  });
  it('should close mobile level 1 menu', () => {
    document.querySelector('a').classList.add('overlay-open');
    spyOn(component, 'mobileMenuSidebarOpen')
      .withArgs(
        {
          target: {
            classList: document.querySelector('a').classList,
          },
        },
        'overlay-open'
      )
      .and.callThrough();
    component.mobileMenuSidebarOpen(
      {
        target: {
          classList: document.querySelector('a').classList,
        },
      },
      'overlay-open'
    );
    expect(component.mobileMenuSidebarOpen).toHaveBeenCalled();
    expect(component.mobileMenuSidebarOpen).toHaveBeenCalledWith(
      {
        target: {
          classList: document.querySelector('a').classList,
        },
      },
      'overlay-open'
    );
  });
});
