import { DOCUMENT } from '@angular/common';
import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  AfterViewInit,
  Renderer2,
  ChangeDetectionStrategy,
} from '@angular/core';
import { RightSidebarService } from 'src/app/core/service/rightsidebar.service';
import { ConfigService } from 'src/app/config/config.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { InConfiguration } from 'src/app/core/models/config.interface';

@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RightSidebarComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit {
  selectedBgColor = 'white';

  maxHeight: string;

  maxWidth: string;

  showpanel = false;

  isOpenSidebar: boolean;

  isDarkSidebar = false;

  isDarTheme = false;

  isRtl = false;

  public config: InConfiguration;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public elementRef: ElementRef,
    private rightSidebarService: RightSidebarService,
    private configService: ConfigService
  ) {
    super();
  }

  ngOnInit(): void {
    this.setConfig();
    this.subs.sink = this.rightSidebarService.sidebarState.subscribe(isRunning => {
      this.isOpenSidebar = isRunning;
    });
    this.setRightSidebarWindowHeight();
  }

  setConfig() {
    this.config = this.configService.config;
  }

  ngAfterViewInit(): void {
    // set header color on startup
    if (localStorage.getItem('choose_skin')) {
      this.renderer.addClass(this.document.body, localStorage.getItem('choose_skin'));
      this.selectedBgColor = localStorage.getItem('choose_skin_active');
    } else {
      this.renderer.addClass(this.document.body, `theme-${this.config.layout.themeColor}`);
      this.selectedBgColor = this.config.layout.themeColor;
    }

    if (localStorage.getItem('menuOption')) {
      this.menuSettings();
    } else {
      this.isDarkSidebar = this.config.layout.sidebar.backgroundColor === 'dark';
    }

    if (localStorage.getItem('theme')) {
      this.themeSettings();
    } else {
      this.isDarTheme = this.config.layout.variant === 'dark';
    }

    if (localStorage.getItem('isRtl')) {
      this.rtlSettings();
    } else {
      this.setLTRSettings();
    }
  }

  rtlSettings() {
    if (localStorage.getItem('isRtl') === 'true') {
      this.setRTLSettings();
    } else if (localStorage.getItem('isRtl') === 'false') {
      this.setLTRSettings();
    }
  }

  themeSettings(): void {
    if (localStorage.getItem('theme') === 'dark') {
      this.isDarTheme = true;
    } else if (localStorage.getItem('theme') === 'light') {
      this.isDarTheme = false;
    } else {
      this.isDarTheme = this.config.layout.variant === 'dark';
    }
  }

  menuSettings(): void {
    if (localStorage.getItem('menuOption') === 'menu_dark') {
      this.isDarkSidebar = true;
    } else if (localStorage.getItem('menuOption') === 'menu_light') {
      this.isDarkSidebar = false;
    } else {
      this.isDarkSidebar = this.config.layout.sidebar.backgroundColor === 'dark';
    }
  }

  selectTheme(color: 'white' | 'black' | 'purple' | 'blue' | 'cyan' | 'green' | 'orange'): void {
    this.selectedBgColor = color;
    const prevTheme = this.elementRef.nativeElement
      .querySelector('.right-sidebar .demo-choose-skin li.actived')
      .getAttribute('data-theme');
    this.renderer.removeClass(this.document.body, `theme-${prevTheme}`);
    this.renderer.addClass(this.document.body, `theme-${this.selectedBgColor}`);
    localStorage.setItem('choose_skin', `theme-${this.selectedBgColor}`);
    localStorage.setItem('choose_skin_active', this.selectedBgColor);
  }

  lightSidebarBtnClick(): void {
    this.renderer.removeClass(this.document.body, 'menu_dark');
    this.renderer.removeClass(this.document.body, 'logo-black');
    this.renderer.addClass(this.document.body, 'menu_light');
    this.renderer.addClass(this.document.body, 'logo-white');
    const menuOption = 'menu_light';
    localStorage.setItem('choose_logoheader', 'logo-white');
    localStorage.setItem('menuOption', menuOption);
  }

  darkSidebarBtnClick(): void {
    this.renderer.removeClass(this.document.body, 'menu_light');
    this.renderer.removeClass(this.document.body, 'logo-white');
    this.renderer.addClass(this.document.body, 'menu_dark');
    this.renderer.addClass(this.document.body, 'logo-black');
    const menuOption = 'menu_dark';
    localStorage.setItem('choose_logoheader', 'logo-black');
    localStorage.setItem('menuOption', menuOption);
  }

  changeThemeBtnClick(theme: 'light' | 'dark'): void {
    const removedTheme = theme === 'light' ? 'dark' : 'light';
    this.renderer.removeClass(this.document.body, removedTheme);
    this.renderer.removeClass(this.document.body, 'submenu-closed');
    this.renderer.removeClass(this.document.body, `menu_${removedTheme}`);
    this.renderer.removeClass(this.document.body, theme === 'light' ? 'logo-black' : 'logo-white');

    if (localStorage.getItem('choose_skin')) {
      this.renderer.removeClass(this.document.body, localStorage.getItem('choose_skin'));
    } else {
      this.renderer.removeClass(this.document.body, `theme-${this.config.layout.themeColor}`);
    }

    this.renderer.addClass(this.document.body, theme);
    this.renderer.addClass(this.document.body, 'submenu-closed');
    this.renderer.addClass(this.document.body, `menu_${theme}`);
    this.renderer.addClass(this.document.body, theme === 'light' ? 'logo-white' : 'logo-black');
    const themeOption = theme === 'light' ? 'white' : 'black';
    this.renderer.addClass(this.document.body, `theme-${themeOption}`);
    const menuOption = `menu_${themeOption}`;
    this.selectedBgColor = theme === 'light' ? 'white' : 'dark';
    this.isDarkSidebar = false;
    localStorage.setItem('choose_logoheader', `logo-${themeOption}`);
    localStorage.setItem('choose_skin', `theme-${themeOption}`);
    localStorage.setItem('theme', theme);
    localStorage.setItem('menuOption', menuOption);
  }

  setRightSidebarWindowHeight(): void {
    const height = window.innerHeight - 137;
    this.maxHeight = `${height}`;
    this.maxWidth = '500px';
  }

  onClickedOutside(event: Event): void {
    const button = event.target as HTMLButtonElement;
    if (button.id !== 'settingBtn') {
      if (this.isOpenSidebar === true) {
        this.toggleRightSidebar();
      }
    }
  }

  toggleRightSidebar(): void {
    this.rightSidebarService.setRightSidebar(!this.isOpenSidebar);
  }

  switchDirection(event: MatSlideToggleChange): void {
    const isrtl = String(event.checked);
    if (isrtl === 'false' && document.getElementsByTagName('html')[0].hasAttribute('dir')) {
      document.getElementsByTagName('html')[0].removeAttribute('dir');
      this.renderer.removeClass(this.document.body, 'rtl');
    } else if (isrtl === 'true' && !document.getElementsByTagName('html')[0].hasAttribute('dir')) {
      document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');
      this.renderer.addClass(this.document.body, 'rtl');
    }
    localStorage.setItem('isRtl', isrtl);
    this.isRtl = event.checked;
  }

  setRTLSettings(): void {
    document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');
    this.renderer.addClass(this.document.body, 'rtl');
    this.isRtl = true;
    localStorage.setItem('isRtl', 'true');
  }

  setLTRSettings(): void {
    document.getElementsByTagName('html')[0].removeAttribute('dir');
    this.renderer.removeClass(this.document.body, 'rtl');
    this.isRtl = false;
    localStorage.setItem('isRtl', 'false');
  }
}
