import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ConfigService } from '@app/config/config.service';
import { RightSidebarService } from 'src/app/core/service/rightsidebar.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
import { By } from '@angular/platform-browser';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { RightSidebarComponent } from './right-sidebar.component';

describe('RightSidebarComponent', () => {
  let component: RightSidebarComponent;
  let fixture: ComponentFixture<RightSidebarComponent>;
  const service = new ConfigService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatSlideToggleModule, MatButtonModule, MatButtonToggleModule],
      declarations: [RightSidebarComponent],
      providers: [RightSidebarService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightSidebarComponent);
    component = fixture.componentInstance;
    localStorage.clear();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should toggle right sidebar if click outside', () => {
    component.isOpenSidebar = true;
    const aside = fixture.debugElement.query(By.css('aside')).nativeElement;
    aside.dispatchEvent(new Event('clickOutside'));
    fixture.detectChanges();
    expect(component.isOpenSidebar).toBeFalse();
  });
  it('should create with local storage item', () => {
    spyOn(component, 'ngAfterViewInit').and.callThrough();
    localStorage.setItem('choose_skin', 'theme-white');
    localStorage.setItem('menuOption', 'menu_dark');
    localStorage.setItem('theme', 'dark');
    localStorage.setItem('isRtl', 'false');
    component.ngAfterViewInit();
    expect(component).toBeTruthy();
  });
  it('should create with local storage item for else path', () => {
    spyOn(component, 'ngAfterViewInit').and.callThrough();
    localStorage.setItem('menuOption', 'menu_light');
    localStorage.setItem('theme', 'light');
    localStorage.setItem('isRtl', 'true');
    component.ngAfterViewInit();
    expect(component).toBeTruthy();
  });
  it('should create with local storage item for wrong value', () => {
    spyOnProperty(service, 'config', 'get').and.returnValue({
      layout: {
        rtl: false, // options:  true & false
        variant: 'light', // options:  light & dark
        themeColor: 'white', // options:  white, black, purple, blue, cyan, green, orange
        logoBgColor: 'white', // options:  white, black, purple, blue, cyan, green, orange
        sidebar: {
          collapsed: true, // options:  true & false
          backgroundColor: 'light', // options:  light & dark
        },
      },
    });
    spyOn(component, 'ngAfterViewInit').and.callThrough();
    localStorage.setItem('menuOption', 'menu_asdasd');
    localStorage.setItem('theme', 'asd');
    component.ngAfterViewInit();
    expect(component).toBeTruthy();
  });
  it('should call setRTLSettings', () => {
    spyOn(component, 'setRTLSettings').and.callThrough();
    component.setRTLSettings();
    expect(component.setRTLSettings).toHaveBeenCalled();
    expect(component.isRtl).toBeTrue();
    expect(localStorage.getItem('isRtl')).toBe('true');
    localStorage.setItem('isRtl', 'false');
  });
  it('should call switchDirection', () => {
    spyOn(component, 'switchDirection').withArgs({ checked: true, source: undefined }).and.callThrough();
    component.switchDirection({ checked: true, source: undefined });
    expect(component.switchDirection).toHaveBeenCalled();
    expect(component.isRtl).toBeTrue();
    expect(localStorage.getItem('isRtl')).toBe('true');
    localStorage.setItem('isRtl', 'false');
  });
  it('should call switchDirection with html element have attributes', () => {
    document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');
    component.isRtl = true;
    spyOn(component, 'switchDirection').withArgs({ checked: false, source: undefined }).and.callThrough();
    component.switchDirection({ checked: false, source: undefined });
    expect(component.switchDirection).toHaveBeenCalled();
    expect(component.isRtl).toBeFalse();
    expect(localStorage.getItem('isRtl')).toBe('false');
    localStorage.setItem('isRtl', 'false');
  });
  it('should toggle right side bar', () => {
    spyOn(component, 'toggleRightSidebar').and.callThrough();
    component.toggleRightSidebar();
    expect(component.toggleRightSidebar).toHaveBeenCalled();
  });
  it('should call dark theme buton click', () => {
    localStorage.setItem('choose_skin', 'theme-white');
    spyOn(component, 'changeThemeBtnClick').withArgs('dark').and.callThrough();
    component.changeThemeBtnClick('dark');
    expect(component.changeThemeBtnClick).toHaveBeenCalled();
  });
  it('should call dark theme buton click without choose_skin', () => {
    spyOn(component, 'changeThemeBtnClick').withArgs('dark').and.callThrough();
    component.changeThemeBtnClick('dark');
    expect(component.changeThemeBtnClick).toHaveBeenCalled();
  });
  it('should call dark sidebar buton click', () => {
    spyOn(component, 'darkSidebarBtnClick').and.callThrough();
    component.darkSidebarBtnClick();
    expect(component.darkSidebarBtnClick).toHaveBeenCalled();
  });
  it('should call light theme btn click', () => {
    localStorage.setItem('choose_skin', 'theme-white');
    spyOn(component, 'changeThemeBtnClick').withArgs('light').and.callThrough();
    component.changeThemeBtnClick('light');
    expect(component.changeThemeBtnClick).toHaveBeenCalled();
  });
  it('should call light theme btn click without choose_skin', () => {
    spyOn(component, 'changeThemeBtnClick').withArgs('light').and.callThrough();
    component.changeThemeBtnClick('light');
    expect(component.changeThemeBtnClick).toHaveBeenCalled();
  });
  it('should call light side bar btn click', () => {
    spyOn(component, 'lightSidebarBtnClick').and.callThrough();
    component.lightSidebarBtnClick();
    expect(component.lightSidebarBtnClick).toHaveBeenCalled();
  });
  it('should change theme if selectTheme called', () => {
    spyOn(component, 'selectTheme').and.callThrough();
    component.selectTheme('white');
    expect(component.selectTheme).toHaveBeenCalled();
    expect(localStorage.getItem('choose_skin')).toBe('theme-white');
  });
});
