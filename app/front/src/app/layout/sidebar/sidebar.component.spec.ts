import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterEvent, Router, NavigationEnd, NavigationStart } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { SidebarComponent } from './sidebar.component';

const eventSubject = new ReplaySubject<RouterEvent>(1);

const routerMock = {
  navigate: jasmine.createSpy('navigate'),
  events: eventSubject.asObservable(),
  url: 'bots/test',
};

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  let router: Router;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      declarations: [SidebarComponent],
      providers: [TranslateService, { provide: Router, useValue: routerMock }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should load sidebar items if user exist', () => {
    expect(component).toBeTruthy();
  });
  it('should be parse url', () => {
    // TODO should be add css class check
    localStorage.setItem('currentUser', JSON.stringify({}));
    localStorage.removeItem('isRtl');
    eventSubject.next(new NavigationEnd(0, routerMock.url, routerMock.url));
    expect(component.level1Menu).toBe('bots');
    expect(component.level2Menu).toBe('test');
  });
  it('should be spinner show', () => {
    localStorage.setItem('currentUser', JSON.stringify({}));
    const menu = { level1: component.level1Menu, level2: component.level2Menu };
    eventSubject.next(new NavigationStart(0, routerMock.url));
    expect(component.level1Menu).toBe(menu.level1);
    expect(component.level2Menu).toBe(menu.level2);
  });
  it('should call window resize', () => {
    spyOn(component, 'windowResizecall').and.callThrough();
    component.windowResizecall();
    expect(component.windowResizecall).toHaveBeenCalled();
  });
  it('should open level 1 menu', () => {
    component.level1Menu = '0';
    spyOn(component, 'callLevel1Toggle')
      .withArgs(
        {
          target: {
            classList: document.querySelector('a').classList,
          },
        },
        '1'
      )
      .and.callThrough();
    component.callLevel1Toggle(
      {
        target: {
          classList: document.querySelector('a').classList,
        },
      },
      '1'
    );
    expect(component.callLevel1Toggle).toHaveBeenCalled();
  });
  it('should close level 1 menu', () => {
    component.level1Menu = '1';
    spyOn(component, 'callLevel1Toggle').and.callThrough();
    document.querySelector('a').classList.add('toggled');
    component.callLevel1Toggle({ target: { classList: document.querySelector('a').classList } }, '1');
    expect(component.callLevel1Toggle).toHaveBeenCalled();
  });
  it('should close level 2 menu', () => {
    component.level2Menu = '2';
    spyOn(component, 'callLevel2Toggle').withArgs('2').and.callThrough();
    component.callLevel2Toggle('2');
    expect(component.callLevel2Toggle).toHaveBeenCalled();
    expect(component.level2Menu).toBe('0');
  });
  it('should open level 2 menu', () => {
    component.level2Menu = '1';
    spyOn(component, 'callLevel2Toggle').withArgs('3').and.callThrough();
    component.callLevel2Toggle('3');
    expect(component.callLevel2Toggle).toHaveBeenCalled();
    expect(component.level2Menu).toBe('3');
  });
  it('should close level 3 menu', () => {
    component.level3Menu = '2';
    spyOn(component, 'callLevel3Toggle').withArgs('2').and.callThrough();
    component.callLevel3Toggle('2');
    expect(component.callLevel3Toggle).toHaveBeenCalled();
    expect(component.level3Menu).toBe('0');
  });
  it('should close level 3 menu', () => {
    component.level3Menu = '1';
    spyOn(component, 'callLevel3Toggle').withArgs('3').and.callThrough();
    component.callLevel3Toggle('3');
    expect(component.callLevel3Toggle).toHaveBeenCalled();
    expect(component.level3Menu).toBe('3');
  });
  it('should call mouse out', () => {
    spyOn(component, 'mouseOut').and.callThrough();
    component.mouseOut();
    expect(component.mouseOut).toHaveBeenCalled();
  });
  it('should call mouse out when side-closed', () => {
    document.querySelector('body').classList.add('side-closed-hover');
    spyOn(component, 'mouseOut').and.callThrough();
    component.mouseOut();
    expect(component.mouseOut).toHaveBeenCalled();
    expect(document.querySelector('body').classList.contains('side-closed-hover')).toBe(false);
    expect(document.querySelector('body').classList.contains('submenu-closed')).toBe(true);
  });
  it('should call mouse hover', () => {
    document.querySelector('body').classList.add('submenu-closed');
    spyOn(component, 'mouseHover').and.callThrough();
    component.mouseHover();
    expect(component.mouseHover).toHaveBeenCalled();
    expect(document.querySelector('body').classList.contains('side-closed-hover')).toBe(true);
    expect(document.querySelector('body').classList.contains('submenu-closed')).toBe(false);
  });
  it('should call mouse hover when submenu not closed', () => {
    document.querySelector('body').classList.remove('submenu-closed');
    spyOn(component, 'mouseHover').and.callThrough();
    component.mouseHover();
    expect(component.mouseHover).toHaveBeenCalled();
  });
  it('should remove ls-closed class if windows innerWidth gerater than 1170', () => {
    spyOnProperty(window, 'innerWidth').and.returnValue(1200);
    window.dispatchEvent(new Event('resize'));
    fixture.detectChanges();
    spyOn(component, 'windowResizecall').and.callThrough();
    component.windowResizecall();
    expect(component.windowResizecall).toHaveBeenCalled();
    expect(document.querySelector('body').classList.contains('ls-closed')).toBe(false);
  });
});
