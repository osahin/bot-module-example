import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '/bots',
    title: 'MENUITEMS.HOME.LIST.BOTLIST',
    moduleName: 'botlist',
    icon: 'view_list',
    class: '',
    groupTitle: false,
    submenu: [],
  },
  {
    path: '/bots/add',
    title: 'MENUITEMS.HOME.LIST.BOTADD',
    moduleName: 'botadd',
    icon: 'add_circle_outline',
    class: '',
    groupTitle: false,
    submenu: [],
  },
];
