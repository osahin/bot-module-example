import { Component, Inject, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalData } from './modal.model';
/**
 * Component
 */
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  /**
   * Input  of modal component
   */
  @Input() icon: string;

  @Input() title: string;

  @Input() message: string;

  @Input() cancelButton: boolean;

  @Input() okButton: boolean;

  @Input() cancelButtonText: string;

  @Input() okButtonText: string;

  filename = 'Filename';

  /**
   * Creates an instance of modal component.
   * @param (MatDialogRef<ModalComponent>) dialog
   * @param (FormBuilder) formBuilder
   * @param (ModalData) data
   */
  constructor(
    public dialog: MatDialogRef<ModalComponent>,
    public formBuilder: UntypedFormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: ModalData
  ) {}

  /**
   * on init
   */
  ngOnInit(): void {
    this.icon = 'warning_amber';
    this.title = 'Warning';
    this.message = 'Emin misin';
    this.cancelButton = true;
    this.okButton = true;
    this.cancelButtonText = 'İptal';
    this.okButtonText = 'Tamam';
  }

  save(): void {
    this.dialog.close(true);
  }

  close(): void {
    this.dialog.close();
  }
}
