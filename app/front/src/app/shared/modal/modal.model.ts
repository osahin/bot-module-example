/**
 * Modal data
 */
export class ModalData {
  icon: string;

  title: string;

  message: string;

  cancelButton: boolean;

  okButton: boolean;

  cancelButtonText?: string;

  okButtonText?: string;
}
