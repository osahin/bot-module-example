import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalData } from './modal-data.model';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {
  buttonText = 'buttonText';

  title = 'title';

  message;

  constructor(public dialog: MatDialogRef<ErrorComponent>, @Inject(MAT_DIALOG_DATA) public data: ModalData) {}

  ngOnInit(): void {
    this.buttonText = this.data.buttonText;
    this.title = this.data.title;
    this.message = this.data.message;
  }

  save(): void {
    this.dialog.close(true);
  }

  close(): void {
    this.dialog.close();
  }
}
