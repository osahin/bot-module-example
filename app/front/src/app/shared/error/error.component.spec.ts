import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';
import { ErrorComponent } from './error.component';

const dialogRefSpyObj = jasmine.createSpyObj({ close: () => {} });

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;
  const dialogMock = {
    close: () => {},
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ErrorComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should be call save', () => {
    spyOn(component, 'save').and.callThrough();
    component.save();
    fixture.detectChanges();
    expect(component.save).toHaveBeenCalled();
  });
  it('should be call dialog', () => {
    spyOn(component, 'close').and.callThrough();
    component.close();
    fixture.detectChanges();
    expect(component.close).toHaveBeenCalled();
  });
});
