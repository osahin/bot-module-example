export class UserCardModel{
    id: number;

    photoURL: string;

    cardName: string;

    description: string;

    title: string;

    comminication_info: string;

    comminication_info2?: string;

    footer_data?: string[];
    
    footer?: string;
}