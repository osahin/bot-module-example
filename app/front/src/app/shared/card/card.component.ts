import { Component, OnInit, Input } from '@angular/core';
import { UserCardModel } from './card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() photoUrl: string;

  @Input() cardName: string;

  @Input() description: string;

  @Input() title: string;

  @Input() comminication_info: string;

  @Input() comminication_info2: string;

  @Input() footer: string;

  @Input() footer_data: string[];

  photo: string;

  control: UserCardModel;

  ngOnInit(): void {
    this.photo = this.photoUrl;
  }
}
