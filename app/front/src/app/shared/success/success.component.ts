import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalData } from '../error/modal-data.model';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss'],
})
export class SuccessComponent implements OnInit {
  buttonText = 'buttonText';

  title = 'title';

  message;

  constructor(public dialog: MatDialogRef<SuccessComponent>, @Inject(MAT_DIALOG_DATA) public data: ModalData) {}

  ngOnInit(): void {
    this.buttonText = this.data.buttonText;
    this.title = this.data.title;
    this.message = this.data.message;
  }

  save(): void {
    this.dialog.close(true);
  }

  close(): void {
    this.dialog.close();
  }
}
