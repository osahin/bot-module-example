export class SignUpUser {
  id?: number;

  password: string;

  email: string;

  updatedAt?: Date;

  createdAt?: Date;
}
