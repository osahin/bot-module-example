export interface InConfiguration {
  layout: {
    rtl: boolean;
    variant: string;
    themeColor: string;
    logoBgColor: string;
    sidebar: {
      collapsed: boolean;
      backgroundColor: string;
    };
  };
}
