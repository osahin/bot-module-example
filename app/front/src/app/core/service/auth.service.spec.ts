import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be login', () => {
    service.login('asd', 'asd').subscribe(data => {
      expect(data).toBeTruthy();
    });
  });
  it('should be register', () => {
    service
      .signup({
        email: '',
        password: '',
      })
      .subscribe(data => {
        expect(data).toBeTruthy();
      });
  });
  it('should be logout', () => {
    service.logout();
    expect(JSON.parse(localStorage.getItem('currentUser'))).toEqual(null);
  });
});
