import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LanguageService } from './language.service';

describe('LanguageService', () => {
  let service: LanguageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
    });
    service = TestBed.inject(LanguageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be created with local storage item', () => {
    localStorage.setItem('lang', 'en');
    expect(service).toBeTruthy();
  });
  it('should be created with local storage not matched item', () => {
    localStorage.setItem('lang', 'fr');
    expect(service).toBeTruthy();
  });
  it('should be set language', () => {
    service.setLanguage('tr');
    expect(service).toBeTruthy();
    expect(localStorage.getItem('lang')).toBe('tr');
  });
});
