import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { User } from '../models/user';
import { SignUpUser } from '../models/signup-user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;

  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string): Observable<User> {
    return this.http
      .post<Record<string, unknown>>('~api/auth/login', {
        username,
        password,
      })
      .pipe(
        map((user: User) => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }

  signup(signUpUser: SignUpUser): Observable<SignUpUser> {
    return this.http.post<SignUpUser>('~api/auth/signup', signUpUser);
  }
  forgotPassword(email: string): Observable<boolean> {
    return this.http.post<boolean>('~api/auth/forget-password', { receiverEmail: email });
  }

  resetPassword(password: string, user_id: number): Observable<boolean> {
    return this.http.put<boolean>(`~api/auth/reset/${user_id}`, { newPassword: password });
  }

  logout(): void {
    this.http.post('~api/auth/logout', {}).subscribe({
      next: () => {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.router.navigateByUrl('/authentication/signin');
      },
    });
  }
}
