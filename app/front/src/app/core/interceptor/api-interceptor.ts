import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  intercept = (
    request: HttpRequest<Record<string, unknown>>,
    next: HttpHandler
  ): Observable<HttpEvent<Record<string, unknown>>> => {
    const apiReq = request.clone({
      url: (request.url as string).replace('~', environment.apiUrl),
    });
    return next.handle(apiReq);
  };
}
