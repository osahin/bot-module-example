import { Component } from '@angular/core';
import { Event, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  currentUrl: string;

  componentWindow: Window | { location: { reload() } };

  constructor(public router: Router, public location: LocationStrategy, private spinner: NgxSpinnerService) {
    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.spinner.show();
        this.location.onPopState(this.reload);
        this.currentUrl = routerEvent.url.substring(routerEvent.url.lastIndexOf('/') + 1);
      }
      if (routerEvent instanceof NavigationEnd) {
        this.spinner.hide();
      }
      window.scrollTo(0, 0);
    });
  }

  // eslint-disable-next-line class-methods-use-this
  reload(): void {
    const compwindow = this.window;
    compwindow.location.reload();
  }

  // eslint-disable-next-line class-methods-use-this
  get window(): Window | { location: { reload() } } {
    return window;
  }
}
