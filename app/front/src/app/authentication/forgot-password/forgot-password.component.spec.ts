import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { ForgotPasswordComponent } from './forgot-password.component';

describe('ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientModule, MatIconModule],
      declarations: [ForgotPasswordComponent],
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have a form with email field', () => {
    expect(component.loginForm.contains('email')).toBeTruthy();
  });
  it('should have a form with email field required', () => {
    const control = component.loginForm.get('email');
    control.setValue('');
    expect(control.valid).toBeFalsy();
    expect(component.controls.email.errors.required).toBeTrue();
  });
  it('should have a form with email field with invalid email', () => {
    const control = component.loginForm.get('email');
    control.setValue('test12345');
    expect(component.controls.email.errors.email).toBeTrue();
    expect(control.valid).toBeFalsy();
    expect(component.controls.email.errors.required).toBeUndefined();
    expect(component.controls.email.errors.minLength).toBeUndefined();
  });
  it('should have a form with email field with invalid email', () => {
    const control = component.loginForm.get('email');
    control.setValue('test');
    expect(component.controls.email.errors.email).toBeTrue();
    expect(control.valid).toBeFalsy();
    expect(component.controls.email.errors.required).toBeUndefined();
    expect(component.controls.email.errors.minlength).toBeTruthy();
  });
  it('should have a form with email field in html template', () => {
    const emailElement = fixture.debugElement.query(By.css('#email')).nativeElement;
    expect(emailElement).toBeTruthy();
    expect(component.controls.email.errors.required).toBeTrue();
  });
  it('should have a form with email invalid errors when email lentgh under 5 ', () => {
    const emailElement = fixture.debugElement.query(By.css('#email')).nativeElement;
    emailElement.value = 'test';
    emailElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const error = component.controls.email.errors;
    expect(error).toBeTruthy();
  });
  it('should have a submit to auth service when valid mail entered', fakeAsync(() => {
    const emailElement = fixture.debugElement.query(By.css('#email')).nativeElement;
    emailElement.value = 'test@test.com';
    emailElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.onSubmit).toHaveBeenCalled();
  }));
  it('should not submit to auth service if form not valid', fakeAsync(() => {
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.onSubmit).toHaveBeenCalled();
  }));
});
