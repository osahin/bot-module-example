import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { Observable } from 'rxjs';
import { AuthService } from '@app/core/service/auth.service';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  loginForm: UntypedFormGroup;

  submitted = false;

  error = '';

  hide = true;

  getState: Observable<{ errorMessage: string | null }>;

  errorMessage: string | null;

  constructor(private formBuilder: UntypedFormBuilder, private router: Router, private service: AuthService) {
    super();
  }

  ngOnInit(): void {
    this.createLoginForm();
  }

  createLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', Validators.required],
    });
  }

  get controls(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    this.error = '';
    if (this.loginForm.valid) {
      this.service.login(this.loginForm.value.username, this.loginForm.value.password).subscribe({
        next: state => {
          if (!state) {
            this.error = 'Kullanıcı Adı veya şifreniz yanlış !';
          } else {
            this.router.navigate(['/bots']);
          }
        },
      });
    } else {
      this.error = 'Kullanıcı Adı veya şifrenizi kontrol ediniz !';
    }
  }
}
