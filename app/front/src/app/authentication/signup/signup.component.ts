import { Component, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/service/auth.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
})
export class SignupComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  loginForm: UntypedFormGroup;

  submitted = false;

  constructor(private formBuilder: UntypedFormBuilder, private router: Router, private service: AuthService) {
    super();
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(5)]],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    });
  }

  get controls(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.loginForm.value.password !== this.loginForm.value.passwordConfirm) {
      return;
    }
    if (!this.loginForm.invalid) {
      this.service.signup({ email: this.loginForm.value.email, password: this.loginForm.value.password }).subscribe({
        next: state => {
          if (state) {
            this.router.navigateByUrl('/bots');
          }
        },
      });
    }
  }
}
