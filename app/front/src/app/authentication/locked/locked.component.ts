import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-locked',
  templateUrl: './locked.component.html',
})
export class LockedComponent implements OnInit {
  loginForm: UntypedFormGroup;

  submitted = false;

  hide = true;

  constructor(private formBuilder: UntypedFormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      password: ['', Validators.required],
    });
  }

  get controls(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    // stop here if form is invalid
    if (!this.loginForm.invalid) {
      this.router.navigate(['/bots']);
    }
  }
}
