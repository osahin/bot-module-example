import { ComponentFixture, fakeAsync, TestBed, waitForAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { LockedComponent } from './locked.component';

describe('LockedComponent', () => {
  let component: LockedComponent;
  let fixture: ComponentFixture<LockedComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'bots', component: LockedComponent }]),
        ReactiveFormsModule,
        MatIconModule,
      ],
      declarations: [LockedComponent],
    }).compileComponents();
  }));
  afterAll(() => {
    fixture.destroy();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(LockedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have a form with password field', () => {
    expect(component.loginForm.contains('password')).toBeTruthy();
  });
  it('should have a form with password field required', () => {
    const control = component.loginForm.get('password');
    control.setValue('');
    expect(control.valid).toBeFalsy();
    expect(component.controls.password.errors.required).toBeTrue();
  });
  it('should have a form with password field in html template', () => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    expect(passwordElement).toBeTruthy();
    expect(component.controls.password.errors.required).toBeTrue();
  });
  it('should have a form with valid password', () => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    passwordElement.value = 'test';
    passwordElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const error = component.controls.password.errors;
    expect(error).toBeNull();
  });
  it('should have a submit to auth service when valid password entered', fakeAsync(() => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    passwordElement.value = 'password1!';
    passwordElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.onSubmit).toHaveBeenCalled();
  }));
  it('should not have a submit to auth service if form invalid', fakeAsync(() => {
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.onSubmit).toHaveBeenCalled();
  }));
});
