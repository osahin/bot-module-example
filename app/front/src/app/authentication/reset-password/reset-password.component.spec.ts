import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { ResetPasswordComponent } from './reset-password.component';

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule, MatIconModule],
      declarations: [ResetPasswordComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have a form with password field', () => {
    expect(component.loginForm.contains('password')).toBeTruthy();
  });
  it('should have a form with password field required', () => {
    const control = component.loginForm.get('password');
    control.setValue('');
    expect(control.valid).toBeFalsy();
    expect(component.controls.password.errors.required).toBeTrue();
  });
  it('should have a form with password field in html template', () => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    expect(passwordElement).toBeTruthy();
    expect(component.controls.password.errors.required).toBeTrue();
  });
  it('should have a form with valid password', () => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    passwordElement.value = 'test';
    passwordElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const error = component.controls.password.errors;
    expect(error).toBeNull();
  });
  it('should have passwords match', () => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    const confirmPasswordElement = fixture.debugElement.query(By.css('#password_repeat')).nativeElement;
    passwordElement.value = 'test';
    passwordElement.dispatchEvent(new Event('input'));
    confirmPasswordElement.value = 'test';
    confirmPasswordElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(component.controls.password_repeat.errors).toBeNull();
    expect(component.controls.password.errors).toBeNull();
    expect(component.loginForm.valid).toBeTrue();
    expect(component.loginForm.errors).toBeNull();
    expect(confirmPasswordElement.value).toEqual(passwordElement.value);
  });
  it('should have a password do not match error if confirm password is empty', fakeAsync(() => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    passwordElement.value = 'password1!';
    passwordElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.onSubmit).toHaveBeenCalled();
    expect(component.error).toEqual('Şifreler Uyuşmuyor');
  }));
  it('should have a submit to auth service when valid passwords entered', fakeAsync(() => {
    const passwordElement = fixture.debugElement.query(By.css('#password')).nativeElement;
    const confirmPasswordElement = fixture.debugElement.query(By.css('#password_repeat')).nativeElement;
    passwordElement.value = 'test';
    passwordElement.dispatchEvent(new Event('input'));
    confirmPasswordElement.value = 'test';
    confirmPasswordElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(component.controls.password_repeat.errors).toBeNull();
    expect(component.controls.password.errors).toBeNull();
    expect(component.loginForm.valid).toBeTrue();
    expect(component.loginForm.errors).toBeNull();
    expect(confirmPasswordElement.value).toEqual(passwordElement.value);
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.onSubmit).toHaveBeenCalled();
    expect(component.error).toBe('');
  }));
});
