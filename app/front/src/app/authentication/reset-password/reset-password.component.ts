import { Component, OnInit } from '@angular/core';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  loginForm: UntypedFormGroup;

  submitted = false;

  error = '';

  hide = true;

  userId;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.userId = this.route.snapshot.paramMap.get('id');
    this.loginForm = this.formBuilder.group({
      password_repeat: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }

  get controls(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    this.error = '';
    if (this.loginForm.invalid) {
      this.error = 'Şifreler Uyuşmuyor';
    } else {
      this.authService.resetPassword(this.controls.password.value, this.userId);
    }
  }
}
