import { ComponentFixture, TestBed, waitForAsync, tick, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Page404Component } from './page404.component';
import { SigninComponent } from '../signin/signin.component';

describe('Page404Component', () => {
  let component: Page404Component;
  let fixture: ComponentFixture<Page404Component>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'authentication/signin', component: SigninComponent }])],
      declarations: [Page404Component],
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(Page404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterAll(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have a button with text Go to home', () => {
    const button = fixture.debugElement.nativeElement.querySelector('button');
    expect(button.textContent).toContain('Go To Home Page');
  });
  it('should have a routing to sign in when click the button', fakeAsync(() => {
    spyOn(component, 'submit').and.callThrough();
    component.submit();
    fixture.detectChanges(); //  method attached to the click.
    const btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click', null);
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    fixture.detectChanges();
    expect(component.submit).toHaveBeenCalled();
  }));
});
