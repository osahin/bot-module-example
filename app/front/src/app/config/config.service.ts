import { Injectable } from '@angular/core';
import { InConfiguration } from '../core/models/config.interface';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  private configData: InConfiguration;

  public get config(): InConfiguration {
    return this.configData;
  }

  constructor() {
    this.setConfigData();
  }

  setConfigData(
    config: InConfiguration = {
      layout: {
        rtl: false, // options:  true & false
        variant: 'light', // options:  light & dark
        themeColor: 'white', // options:  white, black, purple, blue, cyan, green, orange
        logoBgColor: 'white', // options:  white, black, purple, blue, cyan, green, orange
        sidebar: {
          collapsed: false, // options:  true & false
          backgroundColor: 'light', // options:  light & dark
        },
      },
    }
  ): void {
    this.configData = config;
  }
}
